@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 push-md-0">
            <div class="row">
                <div class="col-9">
                    <h2 class="mb-3">News</h2>
                </div>
                <div class="col-3">
                    <a href="{{ route('admin.news.create') }}" class="btn btn-primary" style="float: right">Create</a>
                </div>
            </div>
            <table class="table table-striped bg-white table-bordered">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Title</td>
                        <td>Content</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($news as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->title }}</td>
                        <td>{{ $value->content }}</td>

                        <td style="white-space: nowrap">
                            {{ Form::open(array('url' => 'admin/news/' . $value->id, 'class' => 'float-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <button class="btn btn-sm btn-danger float-right ml-1" type="submit" value="1"><i class="fas fa-fw fa-trash-alt"></i></button>
                            {{ Form::close() }}
                            <a class="btn btn-sm btn-info float-right" href="{{ URL::to('admin/news/' . $value->id . '/edit') }}"><i class="fas fa-fw fa-pencil-alt"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $news->links() }}
        </div>
        <div class="col-md-3 pull-md-9">

        </div>
    </div>
</div>
@endsection
