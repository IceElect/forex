@extends('layouts.app')

@section('content')
    <div class="container">
        {{ Form::model($new, array('route' => array('admin.news.update', $new->id), 'method' => 'PUT')) }}
        <div class="row">
            <div class="col-md-12">
                <h2 class="mb-3">Edit New - {{ $new->title }} </h2>
            </div>
            <div class="col-md-9 push-md-3">
                @include('errors')

                <div class="card mb-3">
                    <div class="card-body">
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('id', 'ID', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('id', Input::old('id'), array('class' => 'form-control', 'disabled' => 'true')) }}</div>
                        </div>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('title', 'Title', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}</div>
                        </div>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('content', 'Content', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::textarea('content', Input::old('content'), array('class' => 'form-control')) }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 pull-md-9">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <a href="{{ route('admin.news.index') }}" class="btn btn-light float-right">Cancel</a>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary float-left')) }}
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>

<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
<script>
    tinymce.init({
        selector:'textarea',
        width: '100%',
        height: 500,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        image_title: true,
        automatic_uploads: true,
        images_upload_url: '/admin/settings/upload',
        file_picker_types: 'image',
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            input.onchange = function() {
                var file = this.files[0];

                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), { title: file.name });
                };
            };
            input.click();
        }
    });
</script>

@endsection
