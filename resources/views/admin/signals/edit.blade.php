@extends('layouts.app')

@section('content')
    <div class="container">
        {{ Form::model($signal, array('route' => array('admin.signals.update', $signal->id), 'method' => 'PUT')) }}
        <div class="row">
            <div class="col-md-12">
                <h2 class="mb-3">Edit signal - {{ $signal->name }} </h2>
            </div>
            <div class="col-md-9 push-md-3">
                @include('errors')

                <div class="card mb-3">
                    <div class="card-body">
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('name', __('Name'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}</div>
                        </div>
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('description', __('Description'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::textarea('description', Input::old('description'), array('class' => 'form-control', 'rows' => 4)) }}</div>
                        </div>

                        <hr>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('price', __('Price'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">
                                <div class="input-group">
                                    {{ Form::number('price', Input::old('price'), array('class' => 'form-control')) }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">USD</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('expiration_days', __('Expiration Days'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::number('expiration_days', Input::old('expiration_days'), array('class' => 'form-control')) }}</div>
                        </div>

                        <hr>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('account_login', __('Account'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('account_login', $signal->account->login, array('class' => 'form-control', 'disabled' => 'disabled')) }}</div>
                        </div>
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('account_broker', __('Account Broker'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('account_broker', $signal->account->broker_server->broker->name, array('class' => 'form-control', 'disabled' => 'disabled')) }}</div>
                        </div>
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('account_server', __('Account Server'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('account_server', $signal->account->broker_server->name, array('class' => 'form-control', 'disabled' => 'disabled')) }}</div>
                        </div>
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('account_online', __('Account Online'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('account_online', $signal->account->online ? 'Yes': 'No', array('class' => 'form-control', 'disabled' => 'disabled')) }}</div>
                        </div>
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('account_online', __('For Broadcast'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('account_online', $signal->account->broadcast ? 'Yes': 'No', array('class' => 'form-control', 'disabled' => 'disabled')) }}</div>
                        </div>
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('account_login', __('Account Password'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('account_login', $signal->account->password, array('class' => 'form-control', 'disabled' => 'disabled')) }}</div>
                        </div>
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('account_login', __('Invest Password'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('account_login', $signal->invest_password, array('class' => 'form-control', 'disabled' => 'disabled')) }}</div>
                        </div>

                        @if(!$signal->account->myfxbook_session)
                        <hr>
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('myfxbook_email', __('Myfxbook Login'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('myfxbook_email', Input::old('myfxbook_email'), array('class' => 'form-control', 'required' => 'false')) }}</div>
                        </div>
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('myfxbook_password', __('Myfxbook Password'), array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('myfxbook_password', Input::old('myfxbook_password'), array('class' => 'form-control w-100', 'required' => 'false', 'type' => 'password')) }}</div>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
            <div class="col-md-3 pull-md-9">
                <div class="card card-body mb-3">
                    <div class="form-group row align-items-center mb-0">
                        <div class="col-md-12">{{ Form::label('status', 'Status', array('class' => 'mb-0')) }}</div>
                        <div class="col-md-12">{{ Form::select('status', array('Waiting', 'Accepted', 'Canceled'), Input::old('status'), array('class' => 'form-control')) }}</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <a href="{{ route('admin.signals.index') }}" class="btn btn-light float-right">Cancel</a>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary float-left')) }}
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
