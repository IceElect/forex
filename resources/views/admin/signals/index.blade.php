@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 push-md-0">
            <div class="row">
                <div class="col-9">
                    <h2 class="mb-3">Signals</h2>
                </div>
                <!-- <div class="col-3">
                    <a href="{{ route('admin.users.create') }}" class="btn btn-primary" style="float: right">Create</a>
                </div> -->
            </div>
            <table class="table table-striped bg-white table-bordered">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>User</td>
                        <td>Status</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($signals as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->name }}</td>
                        <td><a href="/admin/users/{{ $value->user->id }}/edit" target="_blank">{{ $value->user->name }}</a></td>
                        <td>{!! $statuses[$value->status] !!}</td>

                        <td style="white-space: nowrap">
                            {{ Form::open(array('url' => 'admin/signals/' . $value->id, 'class' => 'float-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <button class="btn btn-sm btn-danger float-right ml-1" type="submit" value="1"><i class="fas fa-fw fa-trash-alt"></i></button>
                            {{ Form::close() }}
                            @if($value->account->myfxbook_session)
                                @if ($value->status == 0)
                                {{ Form::open(array('url' => 'admin/signals/' . $value->id, 'class' => 'float-right')) }}
                                    {{ Form::hidden('_method', 'PATCH') }}
                                    <button class="btn btn-sm btn-danger float-right ml-1" type="submit" name="status" value="2"><i class="fas fa-fw fa-times"></i></button>
                                    <button class="btn btn-sm btn-success float-right ml-1" type="submit" name="status" value="1"><i class="fas fa-fw fa-check"></i></button>
                                {{ Form::close() }}
                                @endif
                                <a class="btn btn-sm btn-light float-right" href="{{ URL::to('signals/' . $value->id) }}" target="_blank"><i class="fas fa-fw fa-eye"></i></a>
                            @endif
                            <a class="btn btn-sm btn-info float-right" href="{{ URL::to('admin/signals/' . $value->id . '/edit') }}"><i class="fas fa-fw fa-pencil-alt"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $signals->links() }}
        </div>
        <div class="col-md-3 pull-md-9">

        </div>
    </div>
</div>
@endsection
