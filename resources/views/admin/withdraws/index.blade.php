@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 push-md-0">
            <div class="row">
                <div class="col-9">
                    <h2 class="mb-3">Withdraw Requests</h2>
                </div>
                <!-- <div class="col-3">
                    <a href="{{ route('admin.users.create') }}" class="btn btn-primary" style="float: right">Create</a>
                </div> -->
            </div>
            <table class="table table-striped bg-white table-bordered">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Sum</td>
                        <td>User</td>
                        <td>User Available</td>
                        <td>Requisites</td>
                        <td>Status</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($withdraws as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ abs($value->amount) }}$</td>
                        <td><a href="/admin/users/{{ $value->wallet->holder->id }}/edit" target="_blank">{{ $value->wallet->holder->name }}</a></td>
                        <td width="140px"><span class="@if($value->wallet->holder->withdrawalAvailable() < abs($value->amount) && $value->confirmed == 0) text-danger @endif">{{ $value->wallet->holder->withdrawalAvailable() }}$</span></td>
                        <td>{!! @$value->meta['requisites'] !!}</td>
                        <td>{!! $statuses[$value->confirmed] !!}</td>

                        <td style="white-space: nowrap">
                            {{ Form::open(array('url' => 'admin/withdraws/' . $value->id, 'class' => 'float-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <!-- <button class="btn btn-sm btn-danger float-right ml-1" type="submit" value="1"><i class="fas fa-fw fa-trash-alt"></i></button> -->
                            {{ Form::close() }}
                            @if ($value->confirmed == 0)
                            {{ Form::open(array('url' => 'admin/withdraw/' . $value->id, 'class' => 'float-right')) }}
                                {{ Form::hidden('_method', 'PATCH') }}
                                <button class="btn btn-sm btn-danger float-right ml-1" type="submit" name="confirmed" value="2"><i class="fas fa-fw fa-times"></i></button>
                                <button class="btn btn-sm btn-success float-right ml-1" type="submit" name="confirmed" value="1" @if($value->wallet->holder->withdrawalAvailable() < abs($value->amount)) disabled @endif><i class="fas fa-fw fa-check"></i></button>
                            {{ Form::close() }}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $withdraws->links() }}
        </div>
        <div class="col-md-3 pull-md-9">

        </div>
    </div>
</div>
@endsection
