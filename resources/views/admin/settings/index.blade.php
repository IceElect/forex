@extends('layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="" class="row">
        @csrf
        <div class="col-md-12 push-md-0">
            <div class="row">
                <div class="col-9">
                    <h2 class="mb-3">Settings</h2>
                </div>
                <div class="col-3">
                    <button type="submit" class="btn btn-primary" style="float: right">Save</button>
                </div>
            </div>
            <div class="row">
                @include('errors')
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-body">
                        <div class="form-group">
                            <label class="font-weight-bold">Reward Variant</label>
                            <div class="custom-control custom-radio">
                              <input type="radio" id="reward_variant1" name="reward_variant" value="registered" class="custom-control-input" @if($reward_variant == 'registered') checked @endif>
                              <label class="custom-control-label" for="reward_variant1">When confirming mail</label>
                            </div>
                            <div class="custom-control custom-radio">
                              <input type="radio" id="reward_variant2" name="reward_variant" value="replenishment" class="custom-control-input" @if($reward_variant == 'replenishment') checked @endif>
                              <label class="custom-control-label" for="reward_variant2">When replenishing the balance</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="reward_count" class="font-weight-bold">Reward Need Users Count</label>
                            <input type="number" name="reward_count" id="reward_count" class="form-control" value="{{ $reward_count }}">
                        </div>
                        <div class="form-group">
                            <label for="reward_days" class="font-weight-bold">Reward Signals Days</label>
                            <input type="number" name="reward_days" id="reward_days" class="form-control" value="{{ $reward_days }}">
                        </div>
                        <div class="form-group">
                            <label for="hello" class="font-weight-bold">Hello block</label>
                            <textarea name="hello" id="hello" rows="10" class="form-control">{!! $hello !!}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
<script>
    tinymce.init({
        selector:'textarea#hello',
        width: '100%',
        height: 500,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        image_title: true,
        automatic_uploads: true,
        images_upload_url: '/admin/settings/upload',
        file_picker_types: 'image',
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            input.onchange = function() {
                var file = this.files[0];

                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), { title: file.name });
                };
            };
            input.click();
        }
    });
</script>
@endsection
