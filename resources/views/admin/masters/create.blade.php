@extends('layouts.app')

@section('content')
    <div class="container">
        {{ Form::open(array('url' => 'admin/masters')) }}
        <div class="row">
            <div class="col-md-12">
                <h2 class="mb-3">Create Master</h2>
            </div>
            <div class="col-md-9 push-md-3">
                @include('errors')

                <div class="card mb-3">
                    <div class="card-body">
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('id', 'ID', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('id', Input::old('id'), array('class' => 'form-control', 'disabled' => 'true')) }}</div>
                        </div>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('login', 'Login', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('login', '', array('class' => 'form-control')) }}</div>
                        </div>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('server', 'Server', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('server', '', array('class' => 'form-control')) }}</div>
                        </div>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('currency', 'Currency', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('currency', '', array('class' => 'form-control')) }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 pull-md-9">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <a href="{{ route('admin.users.index') }}" class="btn btn-light float-right">Cancel</a>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary float-left')) }}
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
