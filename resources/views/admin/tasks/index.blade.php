@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 push-md-0">
            <div class="row">
                <div class="col-9">
                    <h2 class="mb-3">Online Tasks</h2>
                </div>
            </div>
            <table class="table table-striped bg-white table-bordered">
                <!-- <thead>
                    <tr>
                        <td>Code</td>
                        <td>Discount</td>
                        <td>Discount Type</td>
                        <td></td>
                    </tr>
                </thead> -->
                <tbody>
                @foreach($accounts as $key => $value)
                    <tr>
                        @if($value->isExpired())
                            <td class="text-danger w-100"><b>Account</b> {{ $value->login }}, <b>Broker</b>: {{ $value->broker_server->broker->name }}, <b>Server</b>: {{ $value->broker_server->name }} , <b>Password</b> {{ $value->password }}, {{ $value->broadcast ? 'For Broadcast' : '' }}</td>
                        @else
                            <td class="text-success w-100"><b>Account</b> {{ $value->login }}, <b>Broker</b>: {{ $value->broker_server->broker->name }}, <b>Server</b>: {{ $value->broker_server->name }} , <b>Password</b> {{ $value->password }}, {{ $value->broadcast ? 'For Broadcast' : '' }}</td>
                        @endif

                        <td class="text-muted text-nowrap"><small>{{ $value->updated_at->format('d M Y H:i') }}</small></td>

                        <td style="white-space: nowrap">
                            {{ Form::open(array('url' => 'admin/tasks/' . $value->id, 'class' => 'float-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <button class="btn btn-sm btn-success float-right ml-1" type="submit" value="1"><i class="fas fa-fw fa-check"></i></button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $accounts->links() }}
        </div>
        <div class="col-md-3 pull-md-9">

        </div>
    </div>
</div>
@endsection
