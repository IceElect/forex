@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 push-md-0">
            <div class="row">
                <div class="col-9">
                    <h2 class="mb-3">Promo</h2>
                </div>
                <div class="col-3">
                    <a href="{{ route('admin.promo.create') }}" class="btn btn-primary" style="float: right">Create</a>
                </div>
            </div>
            <table class="table table-striped bg-white table-bordered">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Code</td>
                        <td>Discount</td>
                        <td>Discount Type</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($promo as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->code }}</td>
                        <td>{{ $value->discount }}</td>
                        <td>{{ $value->discount_type }}</td>

                        <td style="white-space: nowrap">
                            {{ Form::open(array('url' => 'admin/promo/' . $value->id, 'class' => 'float-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <button class="btn btn-sm btn-danger float-right ml-1" type="submit" value="1"><i class="fas fa-fw fa-trash-alt"></i></button>
                            {{ Form::close() }}
                            <a class="btn btn-sm btn-info float-right" href="{{ URL::to('admin/promo/' . $value->id . '/edit') }}"><i class="fas fa-fw fa-pencil-alt"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $promo->links() }}
        </div>
        <div class="col-md-3 pull-md-9">

        </div>
    </div>
</div>
@endsection
