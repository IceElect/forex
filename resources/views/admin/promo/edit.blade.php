@extends('layouts.app')

@section('content')
    <div class="container">
        {{ Form::model($promo, array('route' => array('admin.promo.update', $promo->id), 'method' => 'PUT')) }}
        <div class="row">
            <div class="col-md-12">
                <h2 class="mb-3">Edit Promo - {{ $promo->code }} </h2>
            </div>
            <div class="col-md-9 push-md-3">
                @include('errors')

                <div class="card mb-3">
                    <div class="card-body">
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('id', 'ID', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('id', Input::old('id'), array('class' => 'form-control', 'disabled' => 'true')) }}</div>
                        </div>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('code', 'Code', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('code', Input::old('code'), array('class' => 'form-control')) }}</div>
                        </div>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('discount', 'Discount', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::number('discount', Input::old('discount'), array('class' => 'form-control')) }}</div>
                        </div>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('discount_type', 'Discount Type', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::select('discount_type', array('Fixed', 'Percent'), Input::old('discount_type'), array('class' => 'form-control')) }}</div>
                        </div>

                        <hr>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('date_start', 'Date Start', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9"><input class="form-control" name="date_start" type="dateTime-local" value="{{ $promo->date_start->format('Y-m-d\TH:i') }}"></div>
                        </div>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('date_end', 'Date End', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9"><input class="form-control" name="date_end" type="dateTime-local" value="{{ $promo->date_end->format('Y-m-d\TH:i') }}"></div>
                        </div>

                        <hr>

                        <div class="form-group row align-items-center mb-0">
                            <div class="col-md-3">{{ Form::label('count', 'Count', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::number('count', Input::old('count'), array('class' => 'form-control')) }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 pull-md-9">
                <div class="card card-body mb-3">
                    <div class="form-group row align-items-center mb-0">
                        <div class="col-md-12">{{ Form::label('status', 'Status', array('class' => 'mb-0')) }}</div>
                        <div class="col-md-12">{{ Form::select('status', array('Disabled', 'Active'), Input::old('status'), array('class' => 'form-control')) }}</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <a href="{{ route('admin.promo.index') }}" class="btn btn-light float-right">Cancel</a>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary float-left')) }}
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
