@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 push-md-0">
            <div class="row">
                <div class="col-9">
                    <h2 class="mb-3">Payments Journal</h2>
                </div>
                <!-- <div class="col-3">
                    <a href="{{ route('admin.users.create') }}" class="btn btn-primary" style="float: right">Create</a>
                </div> -->
            </div>
            <table class="table table-striped bg-white table-bordered">
                <thead>
                    <tr>
                        <!-- <td>ID</td> -->
                        <td>Date</td>
                        <td>User</td>
                        <td>Signals</td>
                        <td>Price</td>
                        <td>Comission</td>
                        <!-- <td></td> -->
                    </tr>
                </thead>
                <tbody>
                @foreach($transactions as $key => $value)
                    <tr>
                        <!-- <td>{{ $value->id }}</td> -->
                        <td width="160px">{{ $value->created_at->format('d M Y H:i') }}</td>
                        <td><a href="/admin/users/{{ $value->wallet->holder->id }}/edit" target="_blank">{{ $value->wallet->holder->name }}</a></td>
                        <td>{!! @$value->meta['signals'] !!}</td>
                        <td>{!! abs($value->amount) !!}$</td>
                        <td>{!! @$value->meta['comission'] !!}$</td>

                        <td style="white-space: nowrap">
                            {{ Form::open(array('url' => 'admin/transactions/' . $value->id, 'class' => 'float-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <!-- <button class="btn btn-sm btn-danger float-right ml-1" type="submit" value="1"><i class="fas fa-fw fa-trash-alt"></i></button> -->
                            {{ Form::close() }}
                            @if ($value->confirmed == 0)
                            {{ Form::open(array('url' => 'admin/transactions/' . $value->id, 'class' => 'float-right')) }}
                                {{ Form::hidden('_method', 'PATCH') }}
                                <!-- <button class="btn btn-sm btn-danger float-right ml-1" type="submit" name="confirmed" value="2"><i class="fas fa-fw fa-times"></i></button> -->
                                <!-- <button class="btn btn-sm btn-success float-right ml-1" type="submit" name="confirmed" value="1" @if($value->wallet->holder->transactionsalAvailable() < abs($value->amount)) disabled @endif><i class="fas fa-fw fa-check"></i></button> -->
                            {{ Form::close() }}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $transactions->links() }}
        </div>
        <div class="col-md-3 pull-md-9">

        </div>
    </div>
</div>
@endsection
