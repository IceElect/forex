@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 push-md-0">
            <div class="row">
                <div class="col-9">
                    <h2 class="mb-3">Users</h2>
                </div>
                <div class="col-3">
                    <a href="{{ route('admin.users.create') }}" class="btn btn-primary" style="float: right">Create</a>
                </div>
            </div>
            <table class="table table-striped bg-white table-bordered">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Roles</td>
                        <!-- <td>Account MT4</td> -->
                        <!-- <td>Expire Date</td> -->
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td @if( $value->hasRole('admin') ) style="color: red" @endif )>{{ $value->name }}</td>
                        <td>{{ $value->email }}</td>
                        <td>{{ $value->rolesString() }}</td>
                        <!-- <td>{{ $value->account_mt4 }}</td> -->
                        <!-- <td>{{ $value->expiration_date }}</td> -->

                        <td style="white-space: nowrap">
                            {{ Form::open(array('url' => 'admin/users/' . $value->id, 'class' => 'float-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <button class="btn btn-sm btn-danger float-right ml-1" type="submit" value="1"><i class="fas fa-fw fa-trash-alt"></i></button>
                            {{ Form::close() }}
                            <a class="btn btn-sm btn-info float-right" href="{{ URL::to('admin/users/' . $value->id . '/edit') }}"><i class="fas fa-fw fa-pencil-alt"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $users->links() }}
        </div>
        <div class="col-md-3 pull-md-9">

        </div>
    </div>
</div>
@endsection
