@extends('layouts.app')

@section('content')
    <div class="container">
        {{ Form::open(array('url' => 'admin/users')) }}
        <div class="row">
            <div class="col-md-12">
                <h2 class="mb-3">Create User</h2>
            </div>
            <div class="col-md-9 push-md-3">
                @include('errors')

                <div class="card mb-3">
                    <div class="card-body">
                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('name', 'Name', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('name', '', array('class' => 'form-control')) }}</div>
                        </div>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('email', 'Email', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::email('email', Input::old('email'), array('class' => 'form-control')) }}</div>
                        </div>

                        <!-- <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('expiration_date', 'Expiration Date', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::input('datetime-local', 'expiration_date', '', array('class' => 'form-control')) }}</div>
                        </div> -->

                        <!-- <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('account_mt4', 'Account MT4', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('account_mt4', '', array('class' => 'form-control')) }}</div>
                        </div> -->

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('comment', 'Comment', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::textarea('comment', '', array('class' => 'form-control', 'rows' => 3)) }}</div>
                        </div>

                        <hr>

                        <div class="form-group row align-items-center">
                            <div class="col-md-3">{{ Form::label('password', 'Password', array('class' => 'mb-0')) }}</div>
                            <div class="col-md-9">{{ Form::text('password', Input::old('password'), array('class' => 'form-control')) }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 pull-md-9">
                <div class="card mb-3">
                    <div class="card-header">Roles</div>
                    <div class="card-body">
                        <div class="custom-control custom-checkbox">
                          {{ Form::checkbox('roles[]', 'admin', '', array('class' => 'custom-control-input', 'id' => 'role-admin')) }}
                          {{ Form::label('role-admin', 'Admin', array('class' => 'custom-control-label')) }}
                        </div>
                        <div class="custom-control custom-checkbox">
                          {{ Form::checkbox('roles[]', 'developer', '', array('class' => 'custom-control-input', 'id' => 'role-developer')) }}
                          {{ Form::label('role-developer', 'Developer', array('class' => 'custom-control-label')) }}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <a href="{{ route('admin.users.index') }}" class="btn btn-light float-right">Cancel</a>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary float-left')) }}
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
