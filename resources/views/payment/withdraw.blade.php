@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mb-3">{{ __('Withdraw Request') }}</h3>
                </div>
            </div>

            {{ Form::open(array('url' => route('withdraw') )) }}
            <div class="card mb-3">
                <!-- <div class="card-header font-weight-bold"></div> -->
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-9"><b>You can withdraw {{ Auth::user()->withdrawalAvailable() }} $</b></div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-md-3">{{ Form::label('sum', __('Withdraw Sum'), array('class' => 'mb-0')) }}</div>
                        <div class="col-md-9">{{ Form::number('sum', env('WITHDRAW_MIN_SUM', 50), array('class' => 'form-control', 'max' => Auth::user()->withdrawalAvailable())) }}</div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-md-3">{{ Form::label('requisites', __('Requisites'), array('class' => 'mb-0')) }}</div>
                        <div class="col-md-9">{{ Form::textarea('requisites', Input::old('requisites'), array('class' => 'form-control', 'rows' => 4)) }}</div>
                    </div>

                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-primary">{{ __('Submit Request') }}</button>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}

        </div>
        @include('auth.sidebar')
    </div>
</div>
@endsection
