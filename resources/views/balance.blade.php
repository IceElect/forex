@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mb-3">{{ __('Balance History') }}</h3>
                </div>
            </div>
            <table class="table table-striped2 bg-white table-bordered">
                @foreach($transactions as $transaction)
                @if($transaction->amount > 0)
                    <tr class="text-success">
                        <td style="flex: 1" width="100%"><b>{{ __(@$transaction->meta['description'], $transaction->meta) }}</b></td>
                        <td class="text-nowrap">{{ $transaction->created_at->format('d M Y H:i') }}</td>
                        <td class="text-nowrap">+{{ $transaction->amount }} $</td>
                    </tr>
                @elseif($transaction->type == 'withdraw')
                <tr class="">
                    <td style="flex: 1" class="d-flex"><b style="flex: 1">{{ __($transaction->meta['description'], $transaction->meta) }}</b> {!! @$statuses[$transaction->confirmed] !!}</td>
                    <td class="text-nowrap">{{ $transaction->created_at->format('d M Y H:i') }}</td>
                    <td class="text-nowrap">{{ $transaction->amount }} $</td>
                </tr>
                @else
                    <tr class="text-danger">
                        <td style="flex: 1"><b>{{ __($transaction->meta['description'], $transaction->meta) }}</b></td>
                        <td class="text-nowrap">{{ $transaction->created_at->format('d M Y H:i') }}</td>
                        <td class="text-nowrap">{{ $transaction->amount }} $</td>
                    </tr>
                @endif
                @endforeach
            </table>
        </div>
        @include('auth.sidebar')
    </div>
</div>
@endsection
