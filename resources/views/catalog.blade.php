@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mb-2">
            <div class="row align-items-center">
                @if($is_reward)
                    <div class="col-md-12">
                        <h2 class="mb-0">{{ __('Select Reward') }}</h2>
                        <span class="text-muted mb-3 pl-1">{{ __('You can select any signal on 7 days') }}</span>
                    </div>
                @else
                <div class="col-9">
                    <h2 class="mb-3">{{ __('Catalog') }}</h2>
                </div>
                <div class="col-3">
                    <a href="{{ route('signals.create') }}" class="btn btn-primary" style="float: right"><i class="fa fa-plus fa-fw mr-1"></i> {{ __('Add Signal') }}</a>
                </div>
                @endif
            </div>
        </div>
        <div class="col-md-12">
            @include('errors')
        </div>
        <form method="POST" action="{{ route('catalog.pay') }}" class="col-md-12 push-md-0" id="catalog">
            @csrf
            <div class="card mb-3">
                <div class="card-body pb-0">
                    <table class="w-100 table table-responsive-md text-center catalog-table">
                        <thead>
                            <tr>
                                <th class="text-left">Chart</th>
                                <th>Price</th>
                                <th class="text-left">@sortablelink('name','Name')</th>
                                <th>@sortablelink('growth','Growth')</th>
                                <th>@sortablelink('pips','Pips')</th>
                                <th>@sortablelink('drawdown','DD')</th>
                                <th>@sortablelink('profit_factor','PF')</th>
                                <th>@sortablelink('monthly','Monthly')</th>
                                <th>@sortablelink('daily','Daily')</th>
                                <th>@sortablelink('subscribes_count','Subsrib')</th>
                                <!-- <th>@sortablelink('trades_count','Trades')</th> -->
                                <th>@sortablelink('age','Age')</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($signals as $signal)
                            <tr data-signal="{{$signal->id}}">
                                <td width="150px">
                                    <div class="signal-chart">
                                        <img src="{{$signal->account->chart()}}" alt="">
                                    </div>
                                </td>
                                <td>
                                    <div class="signal-price font-weight-bold hidden">$ {{$signal->price}}</div>
                                    <div class="text-secondary text-nowrap">{{ $signal->expiration_days }} days</div>
                                </td>
                                <td align="left"><a href="{{ route('signals.view', [$signal->id]) }}">{{$signal->name}}</a></td>
                                <td>{{$signal->account->data->growth}}%</td>
                                <td>{{$signal->account->data->pips}}%</td>
                                <td>{{$signal->account->data->drawdown}}</td>
                                <td>{{$signal->account->data->profit_factor}}</td>
                                <td>{{$signal->account->data->monthly}}</td>
                                <td>{{$signal->account->data->daily}}</td>
                                <td>{{$signal->subscribes->count()}}</td>
                                <!-- <td>{{$signal->account->data->trades_count}}</td> -->
                                <td><span class="text-nowrap">{{$signal->account->data->age()}}</span></td>
                                <td>
                                    @if($is_reward)
                                        <a href="{{ route('reward_select', ['signal_id' => $signal->id]) }}" class="btn btn-outline-primary btn-select">{{ __('Select') }}</a>
                                    @else
                                        <input type="checkbox" name="signals[]" value="{{$signal->id}}" hidden>
                                        <button class="btn btn-outline-primary btn-copy">{{ __('Copy') }}</button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="text-center">
                        {{ $signals->links() }}
                    </div>
                </div>
            </div>

            @if(!$is_reward)
            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <div class=""><b>Total:</b> <span data-signals="total">0</span>$</div>
                        </div>
                        <div class="col-md-4">
                            <button type="submit" name="button" class="btn btn-primary btn-block" disabled="disabled">Payment</button>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </form>
        <!-- @include('auth.sidebar') -->
    </div>
</div>

<script type="text/javascript">

</script>

</script>

@endsection
