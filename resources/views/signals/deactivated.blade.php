<div id="deactivatedSignals">
    @if ($deactivatedSignals->count() > 0)
    <h3 class="mb-3">My Deactivated Signals <span class="badge badge-danger">{{$deactivatedSignals->count()}}</span></h3>
    <div class="card mb-4">
        <form method="POST" action="{{ route('signals.activate') }}" class="card-body">
            @csrf
            <div class="row mb-2">
                <div class="col-md-8">
                    <b>{{ __('Name') }}</b>
                </div>
                <div class="col-md-3">
                    <b>{{ __('Expired Date') }}</b>
                </div>
                <div class="col-md-1"></div>
            </div>

            @foreach ($deactivatedSignals as $purchase)
            <div class="row mb-3 align-items-center">
                <div class="col-md-8 mb-2">
                    <input type="text" class="form-control" value="{{$purchase->signal->name}} - {{$purchase->signal->account->data->growth}}% dd-{{$purchase->signal->account->data->drawdown}}%" disabled>
                </div>
                <div class="col-md-3 mb-2">
                    <input type="text" class="form-control" value="{{ $purchase->expiredDate()->format('Y-m-d') }}" disabled>
                </div>
                <div class="col-md-1 mb-2">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck{{ $purchase->id }}" name="purchases[]" value="{{ $purchase->id }}">
                        <label class="custom-control-label" for="customCheck{{ $purchase->id }}"></label>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-success" type="submit" name="submit" value="1" disabled>Activate Signals</button>
                </div>
            </div>
        </form>
    </div>
    @endif
</div>
