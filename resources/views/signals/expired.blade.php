<div id="expiredSignals">
    @if ($expiredSignals->count() > 0)
    <h3 class="mb-3">{{ __('My Expired Signals') }} <span class="badge badge-danger">{{$expiredSignals->count()}}</span></h3>
    <div class="card mb-4">
        <form method="POST" action="{{ route('signals.prolong') }}" class="card-body">
            @csrf
            <input type="hidden" name="prolong" value="1">
            <div class="row mb-2">
                <div class="col-md-8">
                    <b>{{ __('Name') }}</b>
                </div>
                <div class="col-md-3">
                    <b>{{ __('Expired Date') }}</b>
                </div>
                <div class="col-md-1"></div>
            </div>

            @foreach ($expiredSignals as $purchase)
            <div class="row mb-3 align-items-center">
                <div class="col-md-8 mb-2">
                    <input type="text" class="form-control" value="{{$purchase->signal->name}} - {{$purchase->signal->account->data->growth}}% dd-{{$purchase->signal->account->data->drawdown}}%" disabled>
                </div>
                <div class="col-md-3 mb-2">
                    <input type="text" class="form-control" value="{{ $purchase->expiredDate()->format('Y-m-d') }}" disabled>
                </div>
                <div class="col-md-1 mb-2">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck{{ $purchase->id }}" name="purchases[]" value="{{ $purchase->id }}">
                        <label class="custom-control-label" for="customCheck{{ $purchase->id }}"></label>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="row align-items-center">
                <div class="col-md-6">
                    <button class="btn btn-primary" type="submit" name="submit" value="1" disabled>Prolong Signals</button>
                </div>
                <div class="col-md-6 text-right">
                    <div class=""><b>Total:</b> <span data-signals="total">0</span>$</div>
                </div>
            </div>
        </form>
    </div>
    @endif
</div>
