<div id="expiredOnlines">
    @if ($expiredOnlines->count() > 0 || $notexpiredOnlines->count() > 0)
    <h3 class="mb-3">{{ __('My Online Accounts') }} <span class="badge badge-danger">{{$expiredOnlines->count()}}</span></h3>
    <div class="card mb-4">
        @foreach($expiredOnlines as $account)
            <div class="card-body" style="background-color: rgba(255, 0, 0, .2);">
                <div class="row align-items-center">
                    <div class="col-md-7 mb-2 mb-md-0">
                        <input type="text" class="form-control" value="{{ $account->login }}" disabled>
                    </div>
                    <div class="col mb-2 mb-md-0">
                        <input type="text" class="form-control" value="{{ $account->expiredDate()->format('Y-m-d') }}" disabled>
                    </div>
                    <div class="pr-2 mb-2 mb-md-0">
                        <button class="btn btn-primary" data-prolong-account="{{ $account->id }}" type="button" data-toggle="tooltip" data-placement="top" title="{{ __('Prolong') }}"><i class="far fa-fw fa-calendar-plus"></i></button>
                    </div>
                </div>
            </div>
        @endforeach

        @foreach($notexpiredOnlines as $account)
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-md-7 mb-2 mb-md-0">
                        <input type="text" class="form-control" value="{{ $account->login }}" disabled>
                    </div>
                    <div class="col mb-2 mb-md-0">
                        <input type="text" class="form-control" value="{{ $account->expiredDate()->format('Y-m-d') }}" disabled>
                    </div>
                    <div class="pr-2 mb-2 mb-md-0">
                        <button class="btn btn-primary" data-prolong="{{ $account->id }}" type="button" data-toggle="tooltip" data-placement="top" title="{{ __('Prolong') }}"><i class="far fa-fw fa-calendar-plus"></i></button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    @endif
</div>
