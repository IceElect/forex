@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12">
                <a href="{{ route('catalog') }}"><small>< {{ __('Back to Catalog') }}</small></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 mb-1">
                <h1 class="h4 font-weight-bold mb-0">{{ $signal->name }}</h1>
                <small class="text-success text-small font-weight-bold ml-1 mb-1 d-block">
                    <!-- Rank #422, Real, USD, Forex Cheaf -->
                    {{ $signal->marks() }}
                </small>
            </div>
            <form action="{{ route('catalog.pay') }}" method="POST" class="col-md-2 mb-1">
                @csrf
                <input type="hidden" name="signals[]" value="{{ $signal->id }}">
                <button type="submit" class="btn btn-block btn-primary">{{ __('Copy') }}</button>
            </form>
        </div>
        <div class="card mb-3">
            <div class="card-body">{{ $signal->description }}</div>
        </div>
        <div class="row">
            <div class="col-md-4 mb-3">
                <div class="card bg-primary">
                    <div class="card-body text-right text-white">
                        <div class="h1 mb-0">+{{ number_format($signal->account->data->growth, 2) }}%</div>
                        <div>Gain</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="card bg-danger">
                    <div class="card-body text-right text-white">
                        <div class="h1 mb-0">{{ number_format($signal->account->data->drawdown, 2) }}%</div>
                        <div>Drawdown</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="card bg-success">
                    <div class="card-body text-right text-white">
                        <div class="h1 mb-0">+${{ number_format($signal->account->data->profit, 2) }}</div>
                        <div>Profit</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-header">Gain Chart</div>
            <div class="card-body">
                <div style="overflow: hidden">
                    <img src="{{$signal->account->chart(1200)}}" class="img-fluid w-100" style="margin-bottom: -5%" alt="">
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-header">
                <b>Advanced Statistics</b>
                <abbr title="{{ $signal->account->data->updated_at->timezone(env('TIMEZONE'))->format('Y-m-d H:i:s') }}" class="timeago float-right"></abbr>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
                  </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div class="tab-pane active" id="general" role="tabpanel" aria-labelledby="general-tab">
                      <div class="row">
                          <div class="col-md-4 mt-2">
                              <table class="table table-striped table-md w-100">
                                  <tbody>
                                      <tr>
                                          <th>Balance:</th>
                                          <td class="text-right">${{ number_format($signal->account->data->balance, 2) }}</td>
                                      </tr>
                                      <tr>
                                          <th>Deposits:</th>
                                          <td class="text-right">${{ number_format($signal->account->data->deposits, 2) }}</td>
                                      </tr>
                                      <tr>
                                          <th>Withdrawals:</th>
                                          <td class="text-right">${{ number_format($signal->account->data->withdrawals, 2) }}</td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>

                          <div class="col-md-4 mt-2">
                              <table class="table table-striped w-100">
                                  <tbody>
                                      <tr>
                                          <th>Trades</th>
                                          <td class="text-right">{{ $signal->account->data->trades_count }}</td>
                                      </tr>
                                      <tr>
                                          <th>Pips</th>
                                          <td class="text-right">{{ $signal->account->data->pips }}</td>
                                      </tr>
                                      <tr>
                                          <th>Equity</th>
                                          <td class="text-right">${{ number_format($signal->account->data->equity, 2) }}</td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>

                          <div class="col-md-4 mt-2">
                              <table class="table table-striped w-100">
                                  <tbody>
                                      <tr>
                                          <th>Profit Factor</th>
                                          <td class="text-right">{{ $signal->account->data->profit_factor }}</td>
                                      </tr>
                                      <tr>
                                          <th>Daily</th>
                                          <td class="text-right">{{ $signal->account->data->daily }}%</td>
                                      </tr>
                                      <tr>
                                          <th>Monthly</th>
                                          <td class="text-right">{{ $signal->account->data->monthly }}%</td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection
