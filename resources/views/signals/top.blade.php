<div class="fx_row">
    @foreach($signals as $signal)
    <div class="fx_col">
        <div class="fx_card">
            <div class="fx_card-body fx_card-header">
                <h6 class="fx_name">{{ $signal->name }}</h6>
                <b>+{{ $signal->account->data->growth }}%</b>
            </div>
            <div class="fx_graph">
                <img src="{{$signal->account->chart(300, 2)}}" width="100%" alt="{{ $signal->name }}">
            </div>
            <a href="{{ route('signals.view', [$signal->id]) }}" class="fx_card-body">Copy</a>
        </div>
    </div>
    @endforeach
</div>
