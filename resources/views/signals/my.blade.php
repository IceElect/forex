@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 push-md-4">
            <h3 class="mb-3">{{ __('My Signals') }}</h3>

            <div class="card bg-light card-body">
                <div class="row">
                    <div class="col-md-3"><b>Chart</b></div>
                    <div class="col-md-3"><b>Name</b></div>
                    <div class="col-md-2"><b>Status</b></div>
                    <div class="col-md-2"><b>Buys</b></div>
                    <div class="col-md-2 text-center"><b>Total</b></div>
                </div>
            </div>

            @forelse ($signals as $signal)
                <a href="{{ route('signals.view', ['id' => $signal->id]) }}" style="text-decoration: none !important" class="card card-body mt-2">
                    <div class="row align-items-center">
                        <div class="col-md-3">
                            <div class="signal-chart">
                                <img src="{{$signal->account->chart()}}" alt="">
                            </div>
                        </div>
                        <div class="col-md-3 text-truncate"><b>{{$signal->name}}</b></div>
                        <div class="col-md-2">{!! $statuses[$signal->status] !!}</div>
                        <div class="col-md-2"><span><b>{{$signal->buys_count}}</b> / {{$signal->views_count}}</span> <i class="far fa-eye text-muted ml-1"></i></div>
                        <div class="col-md-2 text-center"><b class="">${{$signal->total}}</b></div>
                    </div>
                </a>
            @empty
                <div class="card">
                    <div class="card-body">
                        <b>{{ __('You no have signals. Go to') }} <a href="{{ route('signals.create') }}">{{ ('Create Page') }}</a></b>
                    </div>
                </div>
            @endforelse

        </div>
        @include('auth.sidebar')
    </div>
</div>
@endsection
