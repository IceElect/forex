@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mb-3">{{ __('Add Signal') }}</h3>
                </div>
            </div>

            @include('errors')

            {{ Form::open(array('url' => 'signals/store')) }}
            <div class="card mb-3">
                <!-- <div class="card-header font-weight-bold"></div> -->
                <div class="card-body">
                    <div class="form-group row align-items-center">
                        <div class="col-md-3">{{ Form::label('name', __('Name'), array('class' => 'mb-0')) }}</div>
                        <div class="col-md-9">{{ Form::text('name', '', array('class' => 'form-control')) }}</div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-md-3">{{ Form::label('description', __('Description'), array('class' => 'mb-0')) }}</div>
                        <div class="col-md-9">{{ Form::textarea('description', '', array('class' => 'form-control', 'rows' => 4)) }}</div>
                    </div>

                    <hr>

                    <div class="form-group row">
                        <div class="col-md-3">{{ Form::label('price', __('Price'), array('class' => 'mb-0 col-form-label')) }}</div>
                        <div class="col-md-9">
                            <div class="input-group">
                                {{ Form::text('price', 10, array('class' => 'form-control', 'pattern' => '[0-9]+')) }}
                                <div class="input-group-append">
                                    <span class="input-group-text">USD</span>
                                </div>
                            </div>
                            <small class="form-text text-muted">{!! __('<b>Attention!</b> The service takes a commission of :percent% for each purchase of your signal from the price you specified in the "Price" field', ['percent' => env('COMISSION_PERCENT')]) !!}</small>
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-md-3">{{ Form::label('expiration_days', __('Expiration Days'), array('class' => 'mb-0')) }}</div>
                        <div class="col-md-9">{{ Form::text('expiration_days', 30, array('class' => 'form-control', 'pattern' => '[0-9]+')) }}</div>
                    </div>

                    <hr>

                    <div class="form-group row align-items-center">
                        <div class="col-md-3">{{ Form::label('account_id', __('Account'), array('class' => 'mb-0')) }}</div>
                        <div class="col-md-9">
                            <select name="account_id" id="account_id" class="form-control" required>
                                <option value="" disabled selected>{{ __('Select account') }}</option>
                                @foreach ($accounts as $account)
                                    <option value="{{$account->id}}">{{$account->login}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">{{ Form::label('invest_password', __('Invest Password'), array('class' => 'mb-0 col-form-label')) }}</div>
                        <div class="col-md-9">
                            {{ Form::text('invest_password', '', array('class' => 'form-control', 'required' => 'required')) }}
                            <small class="form-text text-muted">{{ __('Please enter your investment password for your account, this is necessary to update financial statistics.') }}</small>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-primary">{{ __('Submit Request') }}</button>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
        @include('auth.sidebar')
    </div>
</div>
@endsection
