<div class="card mb-3">
    <div class="card-body">
        <table class="w-100 table table-responsive-md text-center catalog-table">
            <thead>
                <tr>
                    <th class="text-left">Chart</th>
                    <th>Price</th>
                    <th class="text-left">Name</th>
                    <!-- <th>Growth</th>
                    <th>Pips</th>
                    <th>DD</th>
                    <th>PF</th> -->
                    <!-- <th>Monthly</th>
                    <th>Daily</th>
                    <th>Subsrib</th>
                    <th>Trades</th>
                    <th>Age</th> -->
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($signals as $key => $signal)
                <tr data-signal="{{$signal->id}}" data-pushace="{{$signal->purchase_id}}">
                    <td width="150px">
                        <div class="signal-chart">
                            <img src="{{$signal->account->chart()}}" alt="">
                        </div>
                    </td>
                    <td>
                        <div class="signal-price font-weight-bold hidden">$ {{$signal->price}}</div>
                        <div class="text-secondary text-nowrap">{{ $signal->expiration_days }} days</div>
                    </td>
                    <td align="left"><a href="{{ route('signals.view', [$signal->id]) }}">{{$signal->name}}</a></td>
                    <!-- <td>{{$signal->account->data->growth}}%</td>
                    <td>{{$signal->account->data->pips}}%</td>
                    <td>{{$signal->account->data->drawdown}}</td>
                    <td>{{$signal->account->data->profit_factor}}</td> -->
                    <!-- <td>{{$signal->account->data->monthly}}</td>
                    <td>{{$signal->account->data->daily}}</td>
                    <td>{{$signal->subscribes->count()}}</td>
                    <td>{{$signal->account->data->trades_count}}</td>
                    <td><span class="text-nowrap">{{$signal->account->data->age()}}</span></td> -->
                    <td class="text-right">
                        @if ($method == 'buy')
                        <button class="btn btn-default btn-remove float-right"><i class="fa fa-times"></i></button>
                        <div class="d-inline-block float-right">
                            <div class="input-group">
                              <span class="input-group-btn">
                                  <button type="button" class="btn btn-default btn-number" data-type="minus" data-field="quant[{{$key}}]" @if($signal->quantity < 2) disabled="disabled" @endif>
                                      <span class="fa fa-minus"></span>
                                  </button>
                              </span>
                              <input type="text" style="max-width: 50px" name="quant[{{$key}}]" class="form-control input-number" value="{{$signal->quantity}}" min="1" max="10">
                              <span class="input-group-btn">
                                  <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[{{$key}}]">
                                      <span class="fa fa-plus"></span>
                                  </button>
                              </span>
                            </div>
                        </div>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
