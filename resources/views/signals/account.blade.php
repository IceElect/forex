@if ($account->subscribes->count() > 0)
<form class="card mb-3" data-account="{{$account->id}}" data-edit="{{intVal($is_edit)}}">
    <div class="card-header d-flex align-items-center">
        <span>Account {{$account->login}}</span>
        <div style="flex: 1"></div>
        @if ($account->percentSum() < 100)
            <button class="btn btn-light btn-sm" disabled>Free Percents - {{ 100 - $account->percentSum() }}%</button>
        @endif
        @if($is_edit != "false")
            <button type="button" class="btn btn-success ml-3 btn-sm" data-toggle-edit="{{$account->id}}" name="submit">
                <i class="fas fa-fw fa-check"></i>
            </button>
        @else
            <button type="button" class="btn btn-primary ml-3 btn-sm" data-toggle-edit="{{$account->id}}" name="submit">
                <i class="fas fa-fw fa-pencil-alt"></i>
            </button>
        @endif
        </button>
    </div>

    <div class="card-body">
        <div class="row mb-2">
            <div class="col-md-4">
                <b>{{ __('Name') }}</b>
            </div>
            <div class="col-md-3">
                <b>{{ __('Expired Date') }}</b>
            </div>
            <div class="col-md-5">
                <b>{{ __('Percent Copy') }}</b>
            </div>
        </div>

        @foreach ($account->subscribes as $subscribe)
        <div class="row mb-3">
            <div class="col-md-4 mb-2">
                <input type="text" class="form-control" value="{{$subscribe->signal->name}} - {{$subscribe->signal->account->data->growth}}% dd-{{$subscribe->signal->account->data->drawdown}}%" disabled>
            </div>
            <div class="col-md-3 mb-2">
                <input type="text" class="form-control" value="{{ $subscribe->expiredDate()->format('Y-m-d') }}" disabled>
            </div>
            <div class="col-md-5 mb-2">
                <div class="d-flex">
                    <div class="input-group">
                        <input type="number" value="{{ $subscribe->percent }}" name="signals[{{$subscribe->id}}]" @if(!$is_edit) disabled @endif class="form-control percent-field">
                        <div class="input-group-append">
                            <span class="input-group-text">%</span>
                        </div>
                    </div>
                    <button class="btn btn-primary ml-4" data-prolong="{{ $subscribe->id }}" type="button" data-toggle="tooltip" data-placement="top" title="{{ __('Prolong') }}"><i class="far fa-fw fa-calendar-plus"></i></button>
                    <button class="btn btn-danger ml-2" data-remove="{{ $subscribe->id }}"><i class="fas fa-fw fa-times"></i></button>
                </div>
            </div>
        </div>
        @endforeach

        @if ($is_edit)
            <div style="clear: both;display: table;"></div>
            <div class="row mt-2">
                <div class="col-md-4 mt-1">
                    <select name="purchase_id" id="purchase_id" class="form-control" required>
                        <option value="" disabled selected>Add signal</option>
                        @foreach ($unusedSignals as $subscribe)
                            <option value="{{$subscribe->id}}">{{$subscribe->signal->name}} - {{$subscribe->signal->account->data->growth}}% dd-{{$subscribe->signal->account->data->drawdown}}%</option>
                        @endforeach
                    </select>
                    @csrf
                </div>
                <div class="col-md-2 mt-1">
                    <button type="submit" name="add_signal" value="1" class="btn btn-success btn-block"><i class="fas fa-plus"></i></button>
                </div>
                <div class="col-md-1 mt-1">

                </div>
                <div class="col-md-5 mt-1">
                    <input type="text" readonly class="form-control-plaintext" name="total_percent" id="staticEmail" value="100 / 100" style="padding: 0.375rem 0.75rem;">
                </div>
            </div>
        @endif

    </div>
</form>
@endif
