@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 push-md-4">

            <div class="row mb-3">
                <div class="col-md-12">
                    @if ($method == 'buy')
                        <h3 class="mb-3">{{ __('Buying ' . count($signals) . ' Signal(s)') }}</h3>
                    @else
                        <h3 class="mb-3">{{ __('Prolong ' . count($signals) . ' Signal(s)') }}</h3>
                    @endif
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-md-12">

                    @include('signals.cart')

                </div>
            </div>
        </div>
        <div class="col-md-4 pull-md-8">
            <div class="row mb-3">
                <div class="col-md-12">
                    <h3 class="mb-3">&nbsp;</h3>
                </div>
            </div>
            <div class="card mb-3">
                <div class="card-body">
                    <dl class="row mb-0">
                        <dt class="col-4">Balance</dt>
                        <dd class="col-8" data-cart="balance">$ <span>{{ Auth::user()->balance }}</span></dd>
                        <dt class="col-4" data-cart="discount" @if (!$discount) style="display: none" @endif>Discount</dt>
                        <dd class="col-8" data-cart="discount" @if (!$discount) style="display: none" @endif>- $ <span>{{ $discount }}</span></dd>
                        <dt class="col-4" data-cart="total">Total</dt>
                        <dd class="col-8" data-cart="total">$ <span>{{ $total }}</span></dd>
                        <dt class="col-4 text-danger" data-cart="shortage" @if (Auth::user()->balance >= $total) style="display: none" @endif>Shortage</dt>
                        <dd class="col-8 text-danger" data-cart="shortage" @if (Auth::user()->balance >= $total) style="display: none" @endif>
                            $ <span>{{ number_format( $total - Auth::user()->balance, 0 ) }}</span>
                            <a href="#" data-toggle="modal" data-target="#exampleModal" class="float-right" data-cart-shortage="{{ $total - Auth::user()->balance }}">{{ __('Top Up Balance') }}</a>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="card mb-3">
                <!-- <div class="card-header">Payment method</div> -->
                <div class="card-body">
                    @if ($promo)
                    <div class="row mb-0">
                        <dt class="col-4">Promo Code</dt>
                        <dd class="col-8 mb-0">{{ $promo->code }}</dd>
                    </div>
                    @else
                    <form class="promo-form d-flex">
                        <input name="code" type="text" placeholder="Promo Code" class="form-control w-100" required>
                        <button type="submit" class="btn btn-secondary ml-3"><i class="fa fa-check"></i></button>
                    </form>
                    @endif
                </div>
            </div>
            <form class="" action="{{ route('cart_buy') }}" method="get">
                <button href="#" class="btn btn-primary btn-block" data-cart="button" @if ( Auth::user()->balance < $total || !$total ) disabled @endif>{{__('Continue')}} <b>$<span>{{$total}}</span></b></button>
            </form>
        </div>
    </div>
</div>
@endsection
