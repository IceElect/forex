@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 push-md-4">

            @include('signals.expired')
            
            @include('signals.deactivated')

            <div style="clear: both;display: table;"></div>

            <h3 class="mb-3">{{ __('My Activated Signals') }}</h3>

            @forelse ($accounts as $account)
                @include('signals.account')
            @empty
                <div class="card">
                    <div class="card-body">
                        <b>{{ __('You no have signals. Go to') }} <a href="{{ route('catalog') }}">{{ ('Catalog') }}</a></b>
                    </div>
                </div>
            @endforelse

        </div>
        @include('auth.sidebar')
    </div>
</div>
@endsection
