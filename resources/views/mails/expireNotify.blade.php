@component('mail::message')
# {{ __('Your Signal Expires in 5 days') }}

{{ __('Your signal ":signal_name" associated with account :account_login will expire in 5 days. Please extend the signal, otherwise it will be unlinked from your account.', ['signal_name' => $signal->name, 'account_login' => $account->login]) }}

@component('mail::button', ['url' => route('signals.index')])
{{ __('Prolong Signal') }}
@endcomponent

{{ __('Thanks, :app_name', ['app_name' => config('app.name')]) }}
@endcomponent
