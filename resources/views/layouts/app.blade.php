<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="min-percent" content="{{ env('MIN_PERCENT') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.4.3/jquery.timeago.min.js"></script>
    <script type="text/javascript">
        var $j = jQuery.noConflict();
        $j(document).ready(function () {
            console.log($j("abbr.timeago").timeago());
        });
    </script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/9270e8f050.js" crossorigin="anonymous"></script>

    <script type="text/javascript">
        var addAccountRefresh = false;
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @can('manage-users')
                        <li class="nav-item {{ (Request::segment(1) == 'admin' && Request::segment(2) == 'signals') ? 'active' : '' }}">
                            <a href="{{ route('admin.signals.index') }}" class="nav-link">Signals</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdownMoney" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ __('Money') }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMoney">
                                <a href="{{ route('admin.withdraw.index') }}" class="dropdown-item">Withdraw</a>
                                <a href="{{ route('admin.payments.index') }}" class="dropdown-item">Payments</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdownMoney" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ __('Objects') }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMoney">
                                <a href="{{ route('admin.users.index') }}" class="dropdown-item">Users</a>
                                <a href="{{ route('admin.news.index') }}" class="dropdown-item">News</a>
                                <a href="{{ route('cron') }}" class="dropdown-item">Sync Myfxbook</a>
                            </div>
                        </li>
                        <li class="nav-item {{ (Request::segment(1) == 'admin' && Request::segment(2) == 'promo') ? 'active' : '' }}">
                            <a href="{{ route('admin.promo.index') }}" class="nav-link">Promo</a>
                        </li>
                        @if(Auth::user()->tasks())
                        <li class="nav-item {{ (Request::segment(1) == 'admin' && Request::segment(2) == 'tasks') ? 'active' : '' }}">
                            <a href="{{ route('admin.tasks.index') }}" class="nav-link">Tasks  <span class="badge badge-primary ml-1" style="top: -2px;position: relative">{{ Auth::user()->tasks() }}</span></a>
                        </li>
                        @endif
                        <li class="nav-item {{ (Request::segment(1) == 'admin' && Request::segment(2) == 'settings') ? 'active' : '' }}">
                            <a href="{{ route('admin.settings') }}" class="nav-link">Settings</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="{{ route('admin.masters.index') }}" class="nav-link">Masters</a>
                        </li> -->
                        @endcan
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item {{ Request::segment(1) == 'login' ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item {{ Request::segment(1) == 'register' ? 'active' : '' }}">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            @if ( Auth::user()->getServicesCount() )
                                <li class="nav-item {{ Request::segment(1) == 'services' ? 'active' : '' }}">
                                    <a href="{{ route('services') }}" class="nav-link text-primary">{{ __('Services') }} <span class="badge badge-danger ml-1" style="top: -2px;position: relative">{{ Auth::user()->getServicesCount() }}</span></a>
                                </li>
                            @endif
                            @if ( Session::has('cart') )
                                @if ( count(Session::get('cart')->items) )
                                <li class="nav-item {{ Request::segment(1) == 'cart' ? 'active' : '' }}">
                                    <a href="{{ route('cart') }}" class="nav-link text-primary">{{ __('Cart') }} <span class="badge badge-primary ml-1" style="top: -2px;position: relative">{{ count(Session::get('cart')->items) }}</span></a>
                                </li>
                                @endif
                            @endif
                            <li class="nav-item {{ Request::segment(1) == 'catalog' ? 'active' : '' }}">
                                <a href="{{ route('catalog') }}" class="nav-link">{{ __('Catalog') }}</a>
                            </li>
                            <li class="nav-item {{ Request::segment(1) == 'signals' ? 'active' : '' }}">
                                <a href="{{ route('signals.my') }}" class="nav-link">{{ __('My Signals') }}</a>
                            </li>
                            <li class="nav-item {{ Request::segment(1) == 'accounts' ? 'active' : '' }}">
                                <a href="{{ route('accounts.list') }}" class="nav-link">{{ __('My Accounts') }}</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a href="#" data-toggle="modal" data-target="#exampleModal" class="dropdown-item d-flex align-items-center" href="{{ route('home') }}">{{ __('Balance') }}: <b class="d-block ml-1 mr-2">$<span data-user="balance">{{ Auth::user()->balance }}</span></b> <span class="badge badge-primary"><i class="fa fa-plus"></i></span></a>
                                    <a class="dropdown-item" href="{{ route('home') }}">{{ __('Profile') }}</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header pb-2 pt-3">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Balance Replenishment') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <form class="row" action="{{ route('payment') }}" method="post">
                      @csrf
                      <div class="col-md-8">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">$</div>
                            </div>
                            <input type="text" name="amountToBePaid" class="form-control" id="sum" value="100" placeholder="Amount">
                          </div>
                      </div>
                      <div class="col-md-4">
                          <button type="submit" class="btn btn-primary btn-block">{{ __('Replenish') }}</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>

        <div class="container mt-5">
            <div class="row">
                <div class="col-md-12">
                    @if(Auth::check())
                        @if(Auth::user()->rewards->count())
                            <div class="alert alert-success">
                                <strong>{{ __('Congratulations!') }}</strong> {{ __('You have :count reward(s)', ['count' => Auth::user()->rewards->count()]) }}
                                <a href="{{ route('reward') }}" class="float-right alert-link">
                                    {{ __('Select') }}
                                </a>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
