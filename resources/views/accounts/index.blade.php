@extends('layouts.app')

@section('content')
<script type="text/javascript">
    var addAccountRefresh = true;
</script>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-9">
                    <h2 class="mb-3">Your Accounts</h2>
                </div>
            </div>
            <table class="table table-striped bg-white table-bordered">
                <thead>
                    <tr>
                        <td>Login</td>
                        <td>Server</td>
                        <td>Currency</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($accounts as $key => $value)
                    <tr>
                        <td width="50%"><b>{{ $value->login }}</b></td>
                        <td>{{ $value->server }}</td>
                        <td>{{ $value->currency }}</td>

                        <td style="white-space: nowrap">
                            {{ Form::open(array('url' => 'accounts/' . $value->id, 'class' => 'float-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                <button class="btn btn-sm btn-danger float-right ml-1" type="submit" value="1"><i class="fas fa-fw fa-trash-alt"></i></button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $accounts->links() }}
        </div>
        @include('auth.sidebar')
    </div>
</div>
@endsection
