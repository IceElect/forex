@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 push-md-4">

            @include('signals.expired_onlines')

            @include('signals.expired')

            <div style="clear: both;display: table;"></div>

            @if( empty($expiredSignals) && empty($expiredOnlines) && empty($notexpiredOnlines))
                <div class="card">
                    <div class="card-body">
                        <b>{{ __('You no have expired services.') }}</b>
                    </div>
                </div>
            @endif

        </div>
        @include('auth.sidebar')
    </div>
</div>
@endsection
