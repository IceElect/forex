@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 push-md-4">
            <h3 class="mb-3">{{ __('Activate Signals') }}</h3>
            <div class="card mb-3">
                <form method="POST" action="{{ route('signals.activate') }}" class="card-body" id="activate">
                    @csrf
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <b>{{ __('Name') }}</b>
                        </div>
                        <div class="col-md-3">
                            <b>{{ __('Expired Date') }}</b>
                        </div>
                        <div class="col-md-3">
                            <b>{{ __('Percent Copy') }}</b>
                        </div>
                    </div>

                    @foreach ($purchases as $purchase)
                    <div class="row mb-2">
                        <div class="col-md-6 mb-2">
                            <input type="text" class="form-control" value="Signal{{$purchase->signal->id}} - {{$purchase->signal->account->data->growth}}% dd-{{$purchase->signal->account->data->drawdown}}%" disabled>
                        </div>
                        <div class="col-md-3 mb-2">
                            <input type="text" class="form-control" value="{{ $purchase->created_at->addDays(15)->format('Y-m-d') }}" disabled>
                        </div>
                        <div class="col-md-3 mb-1">
                            <div class="d-flex">
                                <div class="input-group">
                                    <input type="number" name="purchases[{{ $purchase->id }}]" value="{{ $percent }}" required class="form-control percent-field">
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    <div class="clearfix" style="clear: both;display: table"></div>

                    <div class="row mt-3">
                        <div class="col-md-4 mt-1">
                            <button type="submit" name="submit" value="1" class="btn btn-success btn-block">{{ __('Activate') }}</button>
                        </div>
                        <div class="col-md-4 mt-1">
                            <select name="account_id" id="account_id" class="form-control" required>
                                <option value="" disabled selected>{{ __('Select account') }}</option>
                                @foreach ($accounts as $account)
                                    <option value="{{$account->id}}">{{$account->login}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3 mt-1">
                            <input type="text" readonly class="form-control-plaintext" name="total_percent" id="staticEmail" value="100 / 100" style="padding: 0.375rem 0.75rem;">
                        </div>
                    </div>

                </form>
            </div>
        </div>
        @include('auth.sidebar')
    </div>
</div>
@endsection
