<div class="col-md-4 pull-md-8" id="sidebar">
    <h3 class="mb-3">&nbsp;</h3>
    <div class="card mb-3">
        <div class="card-header">Profile</div>
        <div class="card-body">
            <dl class="row mb-0">
                <dt class="col-md-4">Full Name</dt>
                <dd class="col-md-8">{{ Auth::user()->name }}</dd>

                <dt class="col-md-4">E-mail</dt>
                <dd class="col-md-8">{{ Auth::user()->email }}</dd>

                <dt class="col-md-4">Balance</dt>
                <dd class="col-md-8">
                    {{ Auth::user()->balance }} $
                    <a href="{{ route('balance') }}" class="float-right">History</a>
                </dd>
            </dl>
            @if ( Auth::user()->withdrawalAvailable() )
                @if(Auth::user()->withdrawalAvailable() < env('WITHDRAW_MIN_SUM', 50))
                    <a href="javascript:void(0)" class="btn btn-block btn-primary disabled">{{  __('Withdraw') }} {{Auth::user()->withdrawalAvailable()}} / <b>{{ env('WITHDRAW_MIN_SUM', 50) }}$</b></a>
                @else
                    <a href="{{ route('withdraw') }}" class="btn btn-block btn-primary">{{  __('Withdraw') }} <b>{{Auth::user()->withdrawalAvailable()}}$</b></a>
                @endif
            @endif
        </div>
    </div>
    <form method="POST" action="{{ route('accounts.store') }}" id="addAccount" class="card mb-3">
        @csrf
        <div class="card-header">Add Account</div>
        <div class="card-body">
            <div class="form-group">
                <label for="login">Login</label>
                <input type="text" name="login" id="login" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="server">Broker</label>
                <select name="broker_id" id="broker_id" class="select2 form-control w-100" required>
                    <option selected disabled></option>
                    @foreach(Auth::user()->brokers() as $broker )
                        <option value="{{ $broker->id }}">{{ $broker->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" style="display: none">
                <label for="server">Server</label>
                <select name="server_id" id="server_id" class="select2 form-control w-100" required></select>
            </div>
            <div class="form-group">
                <label for="currency">Currency</label>
                <input type="text" name="currency" id="currency" class="form-control" required>
            </div>
            <hr>
            <div class="form-group">
                <div class="custom-control custom-switch">
                    <input name="online" type="checkbox" class="custom-control-input" id="online">
                    <label class="custom-control-label font-weight-bold" for="online">Online ({{env('PRICE_ONLINE')}}$)</label>
                </div>
            </div>
            <div id="onlineFields" style="display: none">
                <div class="form-group">
                    <label for="password">Account Password</label>
                    <input name="password" type="text" class="form-control" id="password">
                    <small class="form-text text-muted">{{ __('Password from your account required for online.') }}</small>
                </div>
                <div class="custom-control custom-switch">
                    <input name="broadcast" type="checkbox" class="custom-control-input" id="broadcast">
                    <label class="custom-control-label font-weight-" for="broadcast">{{ __('For Broadcast Signal') }}</label>
                </div>
                <hr>
            </div>
            <div class="error mb-2 text-center text-danger"></div>
            <div class="form-group mb-0">
                <button class="btn btn-primary btn-block" type="submit" name="submit" value="1" data-text-online="{{ __('Add Account For ') . env('PRICE_ONLINE') . '$' }}">{{ __('Add Account') }}</button>
            </div>
        </div>
    </form>
    @if (auth()->user()->accounts->count() > 0)
    <div class="card mb-3 signals-list">
        <div class="card-header">My Accounts</div>
        <ul class="list-group list-group-flush">
            @php
                $accounts = auth()->user()->accounts()->orderBy('id', 'desc')->paginate(10);
            @endphp
            @foreach ($accounts as $account)
            <li class="list-group-item"><b>{{ $account->login }}</b> {{ $account->server }} {{ $account->currency }}</li>
            @endforeach
            @if($accounts->total() > 10)
            <a class="list-group-item text-center" href="{{ route('accounts.index') }}">{{ __('See All >') }}</a>
            @endif
        </ul>
    </div>
    @endif
</div>
