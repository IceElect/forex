@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mb-3">{{ __('Service News') }}</h3>
                </div>
            </div>
            @foreach($news as $new)
            <div class="card mb-3">
                <div class="card-header font-weight-bold">{{ $new->title }}</div>
                <div class="card-body">
                    <p class="mb-2">{{ $new->content }}</p>
                    <small class="text-muted">{{ $new->created_at->format('d M Y') }}</small>
                </div>
            </div>
            @endforeach
        </div>
        @include('auth.sidebar')
    </div>
</div>
@endsection
