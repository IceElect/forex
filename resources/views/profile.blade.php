@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="mb-3" data-fx-top></div><script src="/js/widget.js"></script>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 push-md-4">
            <h3>&nbsp;</h3>
            @include('errors')
            <div class="card card-body mb-3">{!! $hello !!}</div>
            <div class="card mb-3">
                <div class="card-header">Referral</div>
                <div class="card-body">
                    <div class="row mb-2 align-items-center">
                        <div class="col-md-3">
                            <b2>Your Referral Link:</b2>
                        </div>
                        <div class="col-md-9">
                            <pre class="pre-code mb-0">{{ Auth::user()->getReferralLink() }}</pre>
                        </div>
                    </div>
                    <div class="row mb-2 align-items-center">
                        <div class="col-md-3">
                            <b2>Count Referrals:</b2>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control-plaintext" value="{{ Auth::user()->referrals()->count() }}" readonly>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-3">
                            <b2>Profit from referrals:</b2>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control-plaintext" value="{{ Auth::user()->referralsProfit() }}$" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-3">
                <div class="card-header">Change Password</div>
                <div class="card-body">
                    {{ Form::model(auth()->user(), array('route' => array('profile.update', auth()->user()->id), 'method' => 'PUT')) }}
                    <div class="form-group row align-items-center">
                        <div class="col-md-3">{{ Form::label('password', 'New Password', array('class' => 'mb-0')) }}</div>
                        <div class="col-md-9">{{ Form::text('password', '', array('class' => 'form-control')) }}</div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-md-3">{{ Form::label('password_confirmation', 'Repeat New Password', array('class' => 'mb-0')) }}</div>
                        <div class="col-md-9">{{ Form::text('password_confirmation', '', array('class' => 'form-control')) }}</div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-md-3"></div>
                        <div class="col-md-9">{{ Form::submit('Save', array('class' => 'btn btn-primary float-left')) }}</div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    Latest News
                    <a href="{{ route('news') }}" class="float-right">All</a>
                </div>
                @foreach ($news as $new)
                <div class="card-body">
                    <h6 class="font-weight-bold">{{ $new->title }}</h6>
                    {!! $new->content !!}
                </div>
                @endforeach
            </div>
        </div>
        @include('auth.sidebar')
    </div>
</div>
@endsection
