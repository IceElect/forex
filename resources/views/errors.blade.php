@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="m-0 p-0 pl-3">
            @foreach($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('message'))
<div class="alert alert-success">
  {!! Session::get('message') !!}
</div>
@endif
