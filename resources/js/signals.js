var Signals = function(){

    var self = {};

    self.init = function(){

        if(addAccountRefresh) {
            $(".signals-list").hide();
        }

        self.minPercent = jQuery('meta[name="min-percent"]').attr('content');
        self.activateForm(jQuery('form#activate'));
        jQuery('form#activate').on('change', function(){
            self.activateForm(this, 10);
        })

        jQuery(document).on('change', 'form[data-account]', function(){
            self.activateForm(this, 10);
        })

        jQuery(document).on('submit', 'form[data-account]', function(e){
            e.preventDefault();

            var form = $(this);
            var is_edit = true;
            var account_id = $(this).data('account');
            var purchase_id = $(this).find('[name=purchase_id]').val();

            $.get('/signals/account', {
                is_edit: is_edit,
                account_id: account_id,
                purchase_id: purchase_id,
            }, function(response){
                e.preventDefault();
                form.replaceWith(response.accountHtml);
                jQuery("#deactivatedSignals").replaceWith(response.deactivatedHtml);

                if(is_edit){
                    self.activateForm(jQuery("form[data-account=" + account_id + "]"));
                }
            })

            return false;
        })

        self.initOnline();

        jQuery(document).on('submit', 'form#addAccount', function(e){
            e.preventDefault();

            var $that = jQuery(this),
            error = $that.find('.error'),
            submit = $that.find('[type=submit]'),
            submit_text = submit.html(),
            formData = new FormData($that.get(0));
            jQuery.ajax({
              url: $that.attr('action'),
              type: 'POST',
              contentType: false,
              processData: false,
              data: formData,
              dataType: 'html',
              beforeSend: function(){
                  error.hide();
                  $that.find(".is-invalid").removeClass('is-invalid');
                  jQuery("body").addClass('loading');
                  submit.html('<i class="fas fa-spin fa-spinner"></i>');
                  submit.attr('disabled', 'disabled');
                  $that.find('.form_result').html('<i class="fa fa-spin fa-refresh"></i>');
              },
              success: function(response){
                  response = JSON.parse(response);

                  if(response.response){
                      if(addAccountRefresh){
                          window.location.reload();
                      }
                      jQuery('#sidebar').replaceWith(response.sidebar);
                      jQuery('select[name="account_id"] option:disabled').after(`<option value='${response.account.id}' selected>${response.account.login}</option>`);
                      self.initOnline();
                  }else{
                      error.html(response.error);
                      error.show();
                  }

                  submit.html(submit_text);
                  submit.attr('disabled', false);
              }
          })

          return false;
        })

        jQuery(document).on('click', '[data-prolong-account]', function(e){
            e.preventDefault();
            var account_id = jQuery(this).data('prolong-account');

            $.ajax({
                type: "PATCH",
                url: '/accounts/' + account_id,
                data: {'account_id': account_id},
                success: function(response) {
                    if(response.response){
                        window.location.reload();
                    }else{
                        alert(response.error);
                    }
                }
            });
        })

        jQuery(document).on('click', '[data-account] [data-prolong]', function(e){
            e.preventDefault();
            var account_card = jQuery(this).closest('[data-account]');
            var purchase_id = jQuery(this).data('prolong');

            jQuery.post('/signals/prolong', {'purchases[0]': purchase_id}, function() {
                window.location.href = '/cart';
            })
        })

        jQuery(document).on('click', '[data-account] [data-remove]', function(e){
            e.preventDefault();

            var account_card = jQuery(this).closest('[data-account]');
            var purchase_id = jQuery(this).data('remove');
            var is_edit = parseInt(account_card.attr('data-edit'));

            $.ajax({
                url: '/signals',
                type: 'DELETE',
                data: {
                    purchase_id: purchase_id,
                    is_edit: is_edit,
                },
                success: function(response) {
                    e.preventDefault();
                    account_card.replaceWith(response.accountHtml);
                    jQuery("#deactivatedSignals").replaceWith(response.deactivatedHtml);

                    if(is_edit){
                        self.activateForm(jQuery("form[data-account=" + client_id + "]"));
                    }
                }
            })

            return false;
        })

        jQuery(document).on('click', '#deactivatedSignals [type=checkbox]', function(e){
            jQuery("#deactivatedSignals [type=submit]").prop('disabled', false);
        })

        jQuery(document).on('click', '#expiredSignals [type=checkbox]', function(e){
            var $that = $(this).closest('form');
            var submit = jQuery("#expiredSignals [type=submit]");
            submit_text = submit.html(),
            formData = new FormData($that.get(0));
            jQuery.ajax({
              url: '/catalog/calc',
              type: 'POST',
              contentType: false,
              processData: false,
              data: formData,
              dataType: 'html',
              beforeSend: function(){
                  $that.find(".is-invalid").removeClass('is-invalid');
                  jQuery("body").addClass('loading');
                  submit.html('<i class="fas fa-spin fa-spinner"></i>');
                  submit.attr('disabled', 'disabled');
                  $that.find('.form_result').html('<i class="fa fa-spin fa-refresh"></i>');
              },
              success: function(data){
                jQuery("body").removeClass('loading');
                jQuery(".error").html("");
                var data = eval('('+data+')');
                if(data.response){
                    $("#expiredSignals [data-signals=total]").parent().removeClass('text-danger');
                    $("#expiredSignals [data-signals=total]").text(data.total);
                    if('redir' in data){
                        window.location.href = data.redir;
                        return false;
                    }
                    if('refresh' in data){
                        location.reload();
                    }
                    if('text' in data){
                        if($that.attr('data-act') == 'login'){
                          window.location.href = data.text;
                        }
                        $that.find('.form_result').html(data.text);
                    }

                    submit.html(submit_text);
                    submit.attr('disabled', false);
                }else{
                    $("#expiredSignals [data-signals=total]").text(data.total);
                    $("#expiredSignals [data-signals=total]").parent().addClass('text-danger');
                    submit.html(submit_text);
                }
                },
                error: function(data){
                    submit.html(submit_text);
                    submit.attr('disabled', false);
                    alert('Произошла ошибка на сервере. Пожалуйста сообщите администратору.')
                }
            });
        })

        jQuery(document).on('click', '[data-account] [data-toggle-edit]', function(e){
            e.preventDefault();

            var card = jQuery(this).closest('form[data-account]');
            var is_edit = !parseInt(card.attr('data-edit'));
            var account_id = card.data('account');

            var data = getFormData(card) || {};
            data.account_id = account_id;
            data.is_edit = is_edit;

            jQuery.get('/signals/account', data, function(response){
                card.replaceWith(response.accountHtml);

                if(is_edit){
                    self.activateForm(jQuery("form[data-account=" + account_id + "]"));
                }
            })
        })
    }

    self.initOnline = function() {
        console.log('online');
        var addAccountForm = $('form#addAccount')
            addAccountSwitch = addAccountForm.find('#online'),
            addAccountButton = addAccountForm.find('button'),
            addAccountButtonTxt = addAccountButton.html();
            addAccountButtonTxtOnline = addAccountButton.data('text-online')
            addAccountBrokerField = addAccountForm.find('#broker_id'),
            addAccountServerField = addAccountForm.find('#server_id');

        jQuery(document).on('select2:change change.select2', 'form#addAccount #broker_id', function(e) {
            var broker_id = addAccountBrokerField.val();
            addAccountServerField.parent().show();
            // jQuery.val(null).empty().trigger('change');

            jQuery.get('/accounts/servers/' + broker_id, (response) => {
                var data = [];
                response.forEach((item) => { data.push({id: item.id, text: item.name}) })
                console.log(data);
                addAccountServerField.select2({
                    theme: 'bootstrap4',
                    data: data
                });
            })
        })

        jQuery(document).on('change', 'form#addAccount #online', function(e){

            addAccountForm.find('#onlineFields').slideToggle(300);
            addAccountForm.find('[name="password"]').toggleAttr('required');

            if($(this).prop('checked')){
                addAccountButton.html(addAccountButtonTxtOnline);
            }else{
                addAccountButton.html(addAccountButtonTxt);
            }
        })
    }

    self.activateForm = function(form, min){
        var $that = jQuery(form);
        var formData = new FormData($that.get(0));
        var percentFields = $that.find('.percent-field');
        var totalPercentField = $that.find('[name=total_percent]');

        var submitButton = $that.find('[name=submit]');

        var totalPercent = 0;
        jQuery.each(percentFields, function(key, item){
            totalPercent += parseFloat(jQuery(item).val());
        })
        totalPercent = Math.round(totalPercent);

        totalPercentField.val(totalPercent + ' / 100');

        if(totalPercent > 100 || totalPercent < min){
            percentFields.addClass('is-invalid');
            totalPercentField.addClass('is-invalid');
            submitButton.prop('disabled', 'disabled');
        }else{
            percentFields.removeClass('is-invalid');
            totalPercentField.removeClass('is-invalid');
            submitButton.prop('disabled', false);
        }
    }

    return self;
}

var signals = new Signals();
signals.init();

function getFormData($form){
    var unindexed_array = $form.serializeArray();
    console.log(unindexed_array);
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}
