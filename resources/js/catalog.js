var Catalog = function(){

    var self = {};

    self.init = function(){
        $("#catalog [data-signal] .btn-copy").on('click', function(e){
            e.preventDefault();
            var signal = $(this).closest('[data-signal]');
            self.toggle(signal);
        })
        // $("#catalog [data-signal] .btn-select").on('click', function(e){
        //     e.preventDefault();
        //     var signal = $(this).closest('[data-signal]'),
        //         signal_id = signal.data('signal');
        //
        //     $.post('/catalog/prolong', {'purchases[0]': signal_id}, function() {
        //         window.location.href = '/cart';
        //     })
        // })
    };
    self.toggle = function(signal){
        var button = signal.find('.btn-copy');
        var checkbox = signal.find('input[type=checkbox]');

        button.toggleClass('btn-outline-primary');
        button.toggleClass('btn-danger');

        checkbox.prop("checked", !checkbox.prop("checked"));

        self.calc();
    };

    self.calc = function(){
        var $that = jQuery("#catalog"),
        submit = $that.find('[type=submit]'),
        checkbox = $that.find('[type=checkbox]'),
        submit_text = submit.html(),
        formData = new FormData($that.get(0));


        jQuery.ajax({
          url: '/catalog/calc',
          type: 'POST',
          contentType: false,
          processData: false,
          data: formData,
          dataType: 'html',
          beforeSend: function(){
              $that.find(".is-invalid").removeClass('is-invalid');
              jQuery("body").addClass('loading');
              submit.attr('disabled', 'disabled');
              submit.html('<i class="fas fa-spin fa-spinner"></i>');
              $that.find('.form_result').html('<i class="fa fa-spin fa-refresh"></i>');
          },
          success: function(data){
            jQuery("body").removeClass('loading');
            jQuery(".error").html("");
            var data = eval('('+data+')');
            if(data.response){
                $("#catalog [data-signals=total]").parent().removeClass('text-danger');
                $("#catalog [data-signals=total]").text(data.total);
                if('redir' in data){
                    window.location.href = data.redir;
                    return false;
                }
                if('refresh' in data){
                    location.reload();
                }
                if('text' in data){
                    if($that.attr('data-act') == 'login'){
                      window.location.href = data.text;
                    }
                    $that.find('.form_result').html(data.text);
                }

                submit.html(submit_text);
                if(data.total > 0) {
                    submit.attr('disabled', false);
                }
            }else{
                $("#catalog [data-signals=total]").text(data.total);
                $("#catalog [data-signals=total]").parent().addClass('text-danger');
                submit.html(submit_text);
                if('errors' in data){
                    $.each(data.errors, function(key, value){
                        var input = $that.find('[name="'+key+'"]'),
                            holder = input.parents('.form-group'),
                            error_h = holder.find('.invalid-feedback');

                        error_h.html(value);
                        input.addClass('is-invalid');
                    });
                }

                submit.attr('disabled', false);
            }
            },
            error: function(data){
                submit.html(submit_text);
                submit.attr('disabled', false);
                alert('Произошла ошибка на сервере. Пожалуйста сообщите администратору.')
            }
        });
    }

    return self;
};
var catalog = new Catalog();
catalog.init();
