require('./bootstrap');
require('./popper');
require('select2');

jQuery.fn.toggleAttr = function(attr) {
    return this.each(function() {
        var $this = $(this);
        $this.attr(attr) ? $this.removeAttr(attr) : $this.attr(attr, attr);
    });
};

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
    }
});

jQuery(document).ready(function() {
    jQuery(document).tooltip({ selector: '[data-toggle=tooltip]' });

    $('.select2').select2({
        theme: 'bootstrap4',
    });
});

jQuery('input[type="number"]').on('keyup',function(){
    v = parseFloat(jQuery(this).val());
    min = parseFloat(jQuery(this).attr('min'));
    max = parseFloat(jQuery(this).attr('max'));

    /*if (v < min){
        $(this).val(min);
    } else */if (v > max){
        jQuery(this).val(max);
    }
})

require('./catalog')
require('./signals')
require('./cart')
