var Cart = function(){
    var self = {};

    self.init = function(){
        $('.promo-form').on('submit', function(e){
            e.preventDefault();

            self.promo(this);

            return false;
        })

        $('[data-cart-shortage]').click(function(){
            var sum = $(this).attr('data-cart-shortage');
            $("#exampleModal input#sum").val(sum);
        })

        //plugin bootstrap minus and plus
        //http://jsfiddle.net/laelitenetwork/puJ6G/
        $('.btn-remove').click(function(e){
            item = $(this).closest('tr')
            signal_id = item.data('signal');
            purchase_id = item.data('purchase');

            $.post('/cart/update', {
                'action': 'remove',
                'signal_id': signal_id,
                'purchase_id': purchase_id
            }, function(data){
                item.remove();
                self.update(data);
            })
        })
        $('.btn-number').click(function(e){
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type      = $(this).attr('data-type');
            var input = $(this).closest('.input-group').find("input[name='"+fieldName+"']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if(type == 'minus') {

                    if(currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if(parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if(type == 'plus') {

                    if(currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if(parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function(){
           $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function() {

            signal_id = $(this).closest('tr').data('signal');
            purchase_id = $(this).closest('tr').data('purchase');

            minValue =  parseInt($(this).attr('min'));
            maxValue =  parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if(valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if(valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }

            $.post('/cart/update', {
                'action': 'quantity',
                'signal_id': signal_id,
                'purchase_id': purchase_id,
                'quantity': $(this).val()
            }, self.update)

        });
    }

    self.update = function(data) {
        balance = $("[data-cart=balance]").find('span').html();

        total = $("[data-cart=total]");
        discount = $("[data-cart=discount]");
        shortage = $("[data-cart=shortage]");
        shortage_attr = $("[data-cart-shortage]");
        button = $("[data-cart=button]");

        total_value = total.find('span');
        discount_value = discount.find('span');
        shortage_value = shortage.find('span');
        button_value = button.find('span');

        total_value.html(data.total);
        button_value.html(data.total);

        if(data.total){
            button.attr('disabled', false);
        }else{
            button.attr('disabled', 'disabled');
        }

        if(data.discount) {
            discount.show();
            discount_value.html(data.discount);
        }else{
            discount.hide();
        }

        if(balance < data.total) {
            shortage.show();
            shortage_value.html(data.total - balance);
            shortage_attr.attr('data-cart-shortage', data.total - balance);
            button.attr('disabled', 'disabled');
        }else{
            shortage.hide();
            if(data.total)
                button.attr('disabled', false);
        }

    }

    self.promo = function(form) {

        var field = $(form).find('input'),
            code = field.val();

        field.removeClass('is-invalid');

        $.get('/promo', {code: code}, function(response){
            // response = JSON.parse(response);

            if(response.response) {
                window.location.reload();
            }else{
                field.addClass('is-invalid');
            }
        })
    }

    return self;
}

var cart = new Cart();
cart.init();
