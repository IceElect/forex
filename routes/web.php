<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->middleware('referral');

Auth::routes(['verify' => true]);

Route::get('/api/top', 'ApiController@top')->name('api.top');
Route::post('/api/order', 'ApiController@order')->name('api.order');
Route::get('/api/account', 'ApiController@account')->name('api.account');

Route::get('/news', 'HomeController@news')->name('news');

Route::put('/home/update', 'HomeController@update')->middleware('auth')->name('profile.update');
Route::get('/code/{code}', 'HomeController@code')->name('profile.code');

Route::group(['middleware' => ['auth','verified']], function() {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/reward', 'ForexController@reward')->name('reward');
    Route::get('/reward/{signal_id}', 'ForexController@reward')->name('reward_select');
    Route::get('/catalog', 'ForexController@catalog')->name('catalog');
    Route::post('/catalog/pay', 'ForexController@catalog_pay')->name('catalog.pay');
    Route::post('/catalog/calc', 'ForexController@catalog_calc')->name('catalog.calc');
    Route::post('/catalog/prolong', 'ForexController@catalog_prolong')->name('catalog.prolong');

    Route::get('/balance', 'ForexController@balance_history')->name('balance');

    Route::get('/withdraw', ['as' => 'withdraw', 'uses' => 'PaymentController@withdraw']);
    Route::post('/withdraw', ['as' => 'withdraw', 'uses' => 'PaymentController@withdraw']);
    Route::post('/payment', ['as' => 'payment', 'uses' => 'PaymentController@payWithpaypal']);
    Route::get('/payment/status',['as' => 'status', 'uses' => 'PaymentController@getPaymentStatus']);
    Route::get('/promo',['as' => 'promo', 'uses' => 'Admin\PromoController@apply', 'namespace' => 'Admin']);

    Route::get('/services', 'ForexController@services')->name('services');

    Route::get('/cart', 'ForexController@cart')->name('cart');
    Route::get('/cart_buy', 'ForexController@cart_buy')->name('cart_buy');
    Route::post('/cart/update', 'ForexController@cart_update')->name('cart_update');

    Route::get('/accounts/list', 'ForexController@accounts')->name('accounts.list');
    Route::get('/accounts/servers/{broker_id}', 'AccountsController@servers')->name('accounts.servers');
    Route::resource('accounts', 'AccountsController');
});

// Route::get('/promo', 'PromoController@apply')->name('promo');

Route::get('/signals/my', 'SignalsController@my')->name('signals.my');
Route::middleware('verified')->prefix('signals')->name('signals.')->group(function () {
    // Route::get('/my', 'SignalsController@my')->name('my');
    Route::get('create', 'SignalsController@create')->name('create');
    Route::post('store', 'SignalsController@store')->name('store');

    Route::get('/account', 'ForexController@signals_account')->name('account');
    Route::delete('', 'ForexController@signals_delete')->name('delete');
    Route::post('/activate', 'ForexController@signals_activate')->name('activate');
    Route::post('/prolong', 'ForexController@signals_prolong')->name('prolong');
});
Route::get('/signals/{id}', 'SignalsController@view')->name('signals.view');

Route::middleware('auth')->group(function () {
    Route::prefix('admin')->namespace('Admin')->name('admin.')->middleware('perm:manage-users')->group(function () {
        Route::resource('users', 'UsersController');
        Route::resource('signals', 'SignalsController');
        Route::resource('withdraw', 'WithdrawController');
        Route::resource('payments', 'PaymentsController');
        Route::resource('masters', 'MastersController');
        Route::resource('promo', 'PromoController');
        Route::resource('news', 'NewsController');
        Route::get('settings', 'SettingsController@index')->name('settings');
        Route::post('settings', 'SettingsController@index');
        Route::post('settings/upload', 'SettingsController@upload');

        Route::get('tasks', 'TasksController@index')->name('tasks.index');
        Route::delete('tasks/{account_id}', 'TasksController@destroy')->name('tasks.destroy');
    });
});

Route::get('/cron', 'CronController@fx')->name('cron');
Route::get('/cron/notify', 'CronController@notify')->name('notify');
