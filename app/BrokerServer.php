<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrokerServer extends Model
{
    protected $fillable = ['name'];

    function broker(){
        return $this->belongsTo('App\Broker');
    }
}
