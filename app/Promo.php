<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    public $table = 'promo';
    protected $dates = ['date_start', 'date_end'];
}
