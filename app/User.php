<?php

namespace App;

use App\Traits\HasRolesAndPermissions;

use App\Broker;

use App\Account;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\DB;

use Bavix\Wallet\Traits\HasWallet;
use Bavix\Wallet\Traits\CanConfirm;
use Bavix\Wallet\Interfaces\Wallet;
use Bavix\Wallet\Interfaces\Confirmable;

use Bavix\Wallet\Models\Transaction;

use Questocat\Referral\Traits\UserReferral;

class User extends Authenticatable implements MustVerifyEmail, Wallet, Confirmable
{
    use Notifiable, HasRolesAndPermissions, HasWallet, UserReferral, CanConfirm;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param mixed ...$roles
     * @return bool
     */
    public function hasRole(... $roles ) {
        foreach ($roles as $role) {
            if ($this->roles->contains('slug', $role)) {
                return true;
            }
        }
        return false;
    }

    public function rolesString() {
        $arr = [];
        foreach($this->roles as $role){
            $arr[] = $role->name;
        }

        return implode(', ', $arr);
    }

    public function expirationDate(){
        return date('Y-m-d\TH:i:s', strtotime($this->expiration_date));
    }

    function accounts(){
        return $this->hasMany('App\Account');
    }

    function usedAccounts(){
        $query = $this->accounts()
            ->leftJoin('signal_user', 'signal_user.account_id', '=', 'accounts.id')
            ->select(['accounts.*', DB::raw('COUNT(signal_user.signal_id) as subscribes_count')])
            ->havingRaw('COUNT(signal_user.account_id) > 0')
            ->groupBy('accounts.id');
        return $query;
    }

    function unusedAccounts(){
        $query = $this->accounts()
            ->leftJoin('signal_user', 'signal_user.account_id', '=', 'accounts.id')
            ->select(['accounts.*', DB::raw('COUNT(signal_user.signal_id) as subscribes_count')])
            ->havingRaw('COUNT(signal_user.account_id) = 0')
            ->groupBy('accounts.id')
            ->orderBy('accounts.created_at', 'desc');
        return $query;
    }

    function unusedAccounts_old(){
        $query = $this->accounts()
            ->leftJoin('subscribes', 'subscribes.client_id', '=', 'accounts.id')
            ->select(['accounts.*', DB::raw('COUNT(subscribes.master_id) as subscribes_count')])
            ->having(DB::raw('COUNT(subscribes.master_id)'), '=', '0')
            ->groupBy('accounts.id');
        return $query;
    }

    function usedAccounts_old(){
        $query = $this->accounts()
            ->leftJoin('subscribes', 'subscribes.client_id', '=', 'accounts.id')
            ->select(['accounts.*', DB::raw('COUNT(subscribes.master_id) as subscribes_count')])
            ->having(DB::raw('COUNT(subscribes.master_id)'), '>', '0')
            ->groupBy('accounts.id');
        return $query;
    }

    function purchases($expired = false){
        // $expired
        $query = $this->hasMany('App\Purchase');
        if(!$expired){
            $query->whereRaw('signal_user.created_at > NOW() - INTERVAL 15 DAY')->orderBy('signal_user.created_at', 'asc');
        }

        return $query;
    }

    function subscribes($only_ids = false){
        $response = array();
        return $response;
        $accounts = $this->accounts;
        foreach($accounts as $account){
            foreach($account->subscribes as $subscribe){
                if($only_ids){
                    $response[] = $subscribe->master_id;
                }else{
                    $response[] = $subscribe;
                }
            }
        }

        return $response;
    }

    function deactivatedSignals(){
        $purchases = $this->purchases->whereNull('account_id');

        return $purchases;
    }

    function expiredSignals(){
        $purchases = $this->purchases(true)
            ->select('signal_user.*')
            ->leftJoin('signals', 'signals.id', '=', 'signal_user.signal_id')
            ->whereRaw('IF(signal_user.is_reward = 0, signal_user.activated_at < NOW() - INTERVAL signals.expiration_days DAY, signal_user.activated_at < NOW() - INTERVAL 7 DAY) AND signal_user.account_id IS NOT NULL')
            ->get();

        return $purchases;
    }

    function referrals() {
        return $this->hasMany('App\User', 'referred_by', 'affiliate_id');
    }

    function affiliate() {
        return $this->hasOne('App\User', 'affiliate_id', 'referred_by');
    }

    function earnedProfit() {
        $sum = $this->wallet->transactions()
            ->whereIn('type', ['earned'])
            ->where('confirmed', 1)
            ->sum('amount');

        return $sum;
    }

    function withdrawSum() {
        $sum = $this->wallet->transactions()
            ->whereIn('type', ['withdraw'])
            ->where('confirmed', 1)
            ->sum('amount');

        return $sum;
    }

    function withdrawalAvailable() {
        return $this->earnedProfit() + $this->withdrawSum();
    }

    function referralsProfit() {
        $sum = $this->wallet->transactions()
            ->whereIn('type', ['referral'])
            ->where('confirmed', 1)
            ->sum('amount');
        // foreach($this->referrals as $referral) {
            // $referral->
        // }

        return $sum;
    }

    function rewards() {
        return $this->hasMany('App\Reward')->where('status', '=', 0);
    }

    function last_reward() {
        return $this->rewards()->orderBy('created_at', 'desc')->first();
    }

    function count_non_rewarded_referrals() {
        return $this->referrals()->where('id', '>', $this->last_reward()->last_referral_id)->get()->count();
    }

    function tasks() {
        return Account::where('task_completed', '=', 0)->where('online', '=', 1)->count();
    }

    function brokers() {
        return Broker::get();
    }

    function expiredOnlines() {
        return $this->accounts()
            ->whereRaw("broadcast = 1 AND updated_at < NOW() - INTERVAL " . env('ONLINE_EXPIRED_DAYS', 15) . " DAY")
            ->get();
    }

    function notexpiredOnlines() {
        return $this->accounts()
            ->whereRaw("broadcast = 1 AND updated_at >= NOW() - INTERVAL " . env('ONLINE_EXPIRED_DAYS', 15) . " DAY")
            ->get();
    }

    function getServicesCount() {
        $signals = $this->purchases(true)
            ->select('signal_user.*')
            ->leftJoin('signals', 'signals.id', '=', 'signal_user.signal_id')
            ->whereRaw('IF(signal_user.is_reward = 0, signal_user.activated_at < NOW() - INTERVAL signals.expiration_days DAY, signal_user.activated_at < NOW() - INTERVAL 7 DAY) AND signal_user.account_id IS NOT NULL')
            ->count();

        $onlines = $this->accounts()
            ->whereRaw("updated_at < NOW() - INTERVAL " . env('ONLINE_EXPIRED_DAYS', 15) . " DAY")
            ->count();

        return $signals + $onlines;
    }
}
