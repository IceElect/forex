<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public $table = 'option';
    protected $fillable = ['value'];
}
