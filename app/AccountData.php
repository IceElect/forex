<?php

namespace App;

use DateTime;

use Illuminate\Database\Eloquent\Model;

class AccountData extends Model
{
    public $table = 'account_data';
    protected $primaryKey = 'account_id';
    // protected $fillable = ['account_id', 'acc_id'];
    protected $guarded = [];

    function age() {
        $tz  = new \DateTimeZone('Europe/Moscow');
        $age = \DateTime::createFromFormat('Y-m-d H:i:s', $this->creation_date, $tz)
             ->diff(new DateTime('now', $tz));

         $response = [];

         if($age->y)
            $response[] = $age->y . 'y';

         if($age->m)
            $response[] = $age->m . 'm';

         if(count($response) < 2)
            $response[] = ($age->d) ? $age->d . 'd' : '';

         return implode(' ', $response);
    }
}
