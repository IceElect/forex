<?php

namespace App\Http;

use App\Option;
use App\Reward;

use Illuminate\Support\Facades\Auth;

class ReferralHelper {

    private $reward_variant;

    function __construct() {
        if(!Auth::check()) {
            exit;
        }
    }

    static function bonus($sum) {
        // dd($sum);
        if(Auth::user()->affiliate){
            $bonus = round($sum / 100 * env('REFERRAL_BONUS_1LVL'));
            Auth::user()->affiliate->deposit($bonus, 'referral', ['description' => 'Profit from Referral :referral_id', 'referral_id' => Auth::user()->id]);
            if(Auth::user()->affiliate->affiliate){
                $bonus2 = $sum / 100 * env('REFERRAL_BONUS_2LVL');
                Auth::user()->affiliate->affiliate->deposit(round($bonus2), 'referral', ['description' => 'Profit from Referral :referral_id', 'referral_id' => Auth::user()->id]);
            }
        }
    }

    static function reward_event($event){
        if(!Auth::check()) {
            exit;
        }

        $user = Auth::user();
        if(!$user->is_rewarded){
            $reward_variant = Option::where(['key' => 'reward_variant'])->first();
            $reward_variant = (!empty($reward_variant)) ? $reward_variant->value : 'replenishment';

            if($user->affiliate){
                if($event == $reward_variant) {
                    // dump('reward');

                    $user->affiliate->increment('for_reward', 1);
                }

                self::reward_check();
            }

            $user->is_rewarded = 1;
            $user->save();
        }
        // dd(Auth::user()->count_non_rewarded_referrals());
    }

    static function reward_check(){
        $af_user = Auth::user()->affiliate;
        $reward_count = Option::where(['key' => 'reward_count'])->first();
        $reward_count = (!empty($reward_count)) ? $reward_count->value : 2;

        if($af_user->for_reward >= $reward_count) {
            $af_user->for_reward = 0;
            $af_user->save();

            $reward = new Reward;
            $reward->user_id = $af_user->id;
            $reward->last_referral_id = Auth::user()->id;
            $reward->save();
        }

        // dump($af_user->for_reward);
    }

}
