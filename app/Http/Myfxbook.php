<?php

namespace App\Http;

use Illuminate\Support\Facades\Http;

class Myfxbook {

    public static $api = 'https://www.myfxbook.com/api/';

    static function login($email, $password) {
        $request = self::_request("login", ['email' => $email, 'password' => $password]);

        if(!$request->error)
            return $request->session;
        return false;
    }

    static function getMyAccounts($session) {
        $request = self::_request("get-my-accounts", ['session' => $session]);
        return $request->accounts;
    }

    static function getAccountInfo($session, $login) {
        $myAccounts = self::getMyAccounts($session);
        foreach($myAccounts as $account) {
            if($account->accountId == $login) {
                return $account;
            }
        }
        return false;
    }

    static function getAccountHistory($session, $accountId) {
        $request = self::_request("get-history", ['session' => $session, 'id' => $accountId]);
        return $request->history;
    }

    static function _request($method, $data){
        $endpoint = self::$api . $method . '.json';
        $endpoint = $endpoint . '?' . http_build_query($data);

        $request = Http::get($endpoint);
        if($request->ok())
            return json_decode($request->body());
        return $request->serverError();
    }

}
