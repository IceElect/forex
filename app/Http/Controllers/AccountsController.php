<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\BrokerServer;

use App\Account;

use Symfony\Component\Console\Input\Input;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Auth::user()->accounts()->paginate(10);
        return view('accounts.index')->with('accounts', $accounts);
    }

    public function servers($broker_id)
    {
        $servers = BrokerServer::where('broker_id', '=', $broker_id)->get();
        return response()->json($servers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error = false;
        $account = Account::where(['login' => $request->get('login'), 'server' => $request->get('server')])->first();
        if ($account) {
            if($account->user_id == Auth::user()->id){
                $error = __('You cannot add 2 identical logins');
            }else{
                $error = __('Impossible to add, contact support');
            }
        }

        if($request->get('online')){
            $price = (int) env('PRICE_ONLINE');
            if(Auth::user()->canWithdraw($price)){
                @Auth::user()->forceWithdraw($price, ['description' => 'Add Account with Online :account_login', 'account_login' => $request->get('login')])->update(['type' => 'online']);
            }else{
                $error = __('Not enough money, top up balance');
            }
        }

        if($error) {
            return response()->json([
                'response' => false,
                'error' => $error
            ]);
        }

        $account = new Account();
        $account->user_id = Auth::user()->id;
        $account->login = $request->get('login');
        $account->server = @$request->get('server');
        $account->server_id = @$request->get('server_id');
        $account->currency = $request->get('currency');
        $account->online = (int) isset($_POST['online']);
        $account->password = $request->get('password');
        $account->broadcast = (int) isset($_POST['broadcast']);
        $result = $account->save();

        return response()->json([
            'response' => $result,
            'account' => $account,
            'sidebar' => view('auth.sidebar')->render()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
     // public function update(Request $request, Account $account) {
     //
     // }

     public function update(Request $request, Account $account)
     {
         $price = env('PRICE_ONLINE', 7);
        if(Auth::user()->canWithdraw($price)){
            @Auth::user()->forceWithdraw($price, ['description' => 'Prolong Account ":login" Online', 'login' => $account->login, 'comission' => $comission])->update(['type' => 'prolong']);
            $account->update(['updated_at' => date('Y-m-d H:i:s')]);
            return response()->json(['response' => true]);
        }else{
            return response()->json(['response' => false, 'error' => __('Not enough money, top up balance')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $account->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // redirect
        Session::flash('message', 'Successfully deleted the account!');
        return Redirect::to('accounts');
    }
}
