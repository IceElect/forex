<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Signal;

use App\Http\Myfxbook;

class SignalsController extends Controller
{
    function __construct() {
        // $this->middleware('auth');
    }

    function view(Request $request, $id){
        $signal = Signal::find($id);

        if(!$signal || !$signal->account->data) {
            abort(404);
        }

        if($signal['status'] != 1 && $signal->account->user_id != Auth::user()->id){
            abort(404);
        }

        Signal::where('id', '=', $id)->increment('views_count', 1);

        return view('signals.view')->with('signal', $signal);
    }

    function my(Request $request) {
        $transactions = Auth::user()->transactions()->where('type', '=', 'earned')->get();
        $signals = Signal::leftJoin('accounts', 'signals.account_id', '=', 'accounts.id')
            ->select('signals.*')
            ->where('accounts.user_id', '=',  Auth::user()->id)->get();

        foreach($signals as $key => $signal) {
            $signals[$key]->total = $this->_find($transactions, $signal->id);
        }

        return view('signals.my')->with([
            'signals' => $signals,
            'statuses' => [
                '<span class="text-muted">Waiting</span>',
                '<span class="text-success">Accepted</span>',
                '<span class="text-danger">Declined</span>',
            ],
        ]);
    }

    function _find($transactions, $signal_id) {
        $sum = 0;
        foreach($transactions as $transaction) {
            if($transaction->meta['signal_id'] == $signal_id) $sum += $transaction->amount;
        }
        return $sum;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accounts = Auth::user()->accounts;
        return view('signals.create')->with([
            'accounts' => $accounts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|integer',
            'expiration_days' => 'required|integer',
            'account_id' => 'required|integer',
            'invest_password' => 'required',
            // 'myfxbook_email' => 'required',
            // 'myfxbook_password' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);


        // $token = "lcZXl2EJUmEqPI2RBD6a543814";

        $myfxbook_error = false;


        // process the code
        if ($validator->fails() || $myfxbook_error) {
            if($myfxbook_error) {
                $validator->errors()->add('myfxbook_email', 'Myfxbook account not found');
                $validator->getMessageBag()->add('myfxbook_email', 'Myfxbook account not found');
            }
            return Redirect::to('signals/create')
                ->withErrors($validator->errors())
                ->withInput($request->all());
        } else {

            $signal = new Signal;
            $signal->name = $request->get('name');
            $signal->description = $request->get('description');
            $signal->price = (int) $request->get('price');
            $signal->expiration_days = (int) $request->get('expiration_days');
            $signal->account_id = $request->get('account_id');
            $signal->invest_password = $request->get('invest_password');
            $signal->save();

            // redirect
            Session::flash('message', 'Successfully submited request!');
            return Redirect::to('signals/my')->with('message', 'Successfully submited request!');
        }
    }
}
