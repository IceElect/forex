<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Option;

class SettingsController extends Controller
{
    function index(Request $request){

        $hello = $this->_getOpt('hello');
        $reward_days = $this->_getOpt('reward_days', 7);
        $reward_count = $this->_getOpt('reward_count', 1);
        $reward_variant = $this->_getOpt('reward_variant', 'replenishment');

        return view('admin.settings.index')->with([
            'hello' => $hello,
            'reward_days' => $reward_days,
            'reward_count' => $reward_count,
            'reward_variant' => $reward_variant,
        ]);
    }

    public function upload()
    {
        $imgpath = '/' . request()->file('file')->store('uploads', 'local');
        return json_encode(['location' => $imgpath]);
    }

    function _getOpt($name, $default_value = '') {
        if(request()->get($name)){
            $value = request()->get($name);

            $option = Option::where(['key' => $name])->first();
            if(!$option) {
                $option = new Option;
                $option->key = $name;
            }

            $option->value = $value;
            $option->save();

        }else{
            $option = Option::where(['key' => $name])->first();
            if(!empty($option)){
                @$value = $option->value;
            }else {
                $value = $default_value;
            }
        }

        return $value;
    }
}
