<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Symfony\Component\Console\Input\Input;

use Bavix\Wallet\Models\Transaction;

class WithdrawController extends Controller
{

    private $statuses = [
        '<span class="text-muted">Waiting</span>',
        '<span class="text-success">Accepted</span>',
        '<span class="text-danger">Declined</span>',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $withdraws = Transaction::where(['type' => 'withdraw'])->orderBy('id', 'desc')->paginate(20);
        return view('admin.withdraws.index')->with([
            'withdraws' => $withdraws,
            'statuses' => $this->statuses,
        ]);
    }

    function update(Request $request, $transaction_id) {
        $transaction = Transaction::find($transaction_id);
        if($request->get('confirmed') == 1) {
            $transaction->wallet->holder->confirm($transaction);
        }else{
            $transaction->update(['confirmed' => 2]);
        }
        return Redirect::to('admin/withdraw');
    }
}
