<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use App\Master;

class MastersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $masters = Master::orderBy('id', 'desc')->paginate(10);
        return view('admin.masters.index')->with('masters', $masters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.masters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'login' => 'integer',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/masters/create')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            // store
            $master = new Master;
            $master->login       = $request->get('login');
            $master->server      = $request->get('server');
            $master->currency    = $request->get('currency');
            $master->save();

            // redirect
            Session::flash('message', 'Successfully created master!');
            return Redirect::to('admin/masters');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $master = Master::find($id);

        return view('admin.masters.show')
           ->with('master', $master);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $master = Master::find($id)->first();

        // show the edit form and pass the nerd
        return view('admin.masters.edit')
            ->with('master', $master);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'login' => 'integer',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/masters/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            // store
            $master = Master::find($id);
            $master->login       = $request->get('login');
            $master->server      = $request->get('server');
            $master->currency    = $request->get('currency');
            $master->save();

            // redirect
            Session::flash('message', 'Successfully updated master!');
            return Redirect::to('admin/masters');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $master = Master::find($id)->first();
        $master->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the master!');
        return Redirect::to('admin/masters');
    }
}
