<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use Bavix\Wallet\Models\Transaction;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aTransactions = Transaction::where([
            'type' => 'buy',
        ])->orderBy('id', 'desc');
        $transactions = $aTransactions->paginate(20);
        $transactionsSum = $aTransactions->sum('amount');
        return view('admin.payments.index')->with([
            'transactions' => $transactions,
            'transactions_sum' => $transactionsSum,
            'statuses' => [
                '<span class="text-muted">Waiting...</span>',
                '<span class="text-success">Accepted</span>',
                '<span class="text-danger">Declined</span>',
            ],
        ]);
    }
}
