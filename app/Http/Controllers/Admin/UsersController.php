<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Symfony\Component\Console\Input\Input;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'desc')->paginate(10);
        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dump(Request::get('name'));

        $rules = array(
            'name'       => 'required',
            'email'      => 'required|email',
            'password'   => 'required',
            // 'account_mt4'      => 'required',
            // 'nerd_level' => 'required|numeric'
        );
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/users/create')
                ->withErrors($validator)
                ->withInput($request->except('password'));
        } else {
            // store
            $user = new User;
            $user->name       = $request->get('name');
            $user->email      = $request->get('email');

            $user->comment     = $request->get('comment');
            // $user->account_mt4 = $request->get('account_mt4');

            // $user->expiration_date = date('Y-m-d H:i:s', strtotime($request->get('expiration_date')));

            $user->password   = Hash::make('1');
            // $users->nerd_level = Input::get('nerd_level');
            $user->save();

            if(!empty($request->get('roles'))) {
                foreach($request->get('roles') as $key => $slug){
                    $user->roles()->attach(Role::where('slug', $slug)->first());
                }
            }

            // redirect
            Session::flash('message', 'Successfully created user!');
            return Redirect::to('admin/users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = User::find($user);

        return view('admin.users.show')
           ->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // $user = User::find($user);

        // show the edit form and pass the nerd
        return view('admin.users.edit')
            ->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $rules = array(
            'name'       => 'required',
            'email'      => 'required|email',
            // 'account_mt4'      => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/users/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->except('password'));
        } else {
            // store
            $user = User::find($id);
            $user->name        = $request->get('name');
            $user->email       = $request->get('email');
            $user->comment     = $request->get('comment');
            // $user->account_mt4 = $request->get('account_mt4');

            // $user->expiration_date = date('Y-m-d H:i:s', strtotime($request->get('expiration_date')));
            // dd($user->expiration_date);

            if(!empty($request->get('password'))) {
                $user->password = Hash::make($request->get('password'));
            }

            $user->save();

            $user->roles()->detach();
            if(!empty($request->get('roles'))) {
                foreach($request->get('roles') as $key => $slug){
                    $user->roles()->attach(Role::where('slug', $slug)->first());
                }
            }

            // redirect
            Session::flash('message', 'Successfully updated user!');
            return Redirect::to('admin/users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // $user = User::find($user);
        $user->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the user!');
        return Redirect::to('admin/users');
    }
}
