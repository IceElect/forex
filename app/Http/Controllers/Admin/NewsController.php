<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use App\News;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('id', 'desc')->paginate(10);
        return view('admin.news.index')->with('news', $news);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            // 'title' => 'integer',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the title
        if ($validator->fails()) {
            return Redirect::to('admin/news/create')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            // store
            $new = new News;
            $new->title       = $request->get('title');
            $new->content      = $request->get('content');
            $new->save();

            // redirect
            Session::flash('message', 'Successfully created new!');
            return Redirect::to('admin/news');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new = News::find($id);

        return view('admin.news.show')
           ->with('new', $new);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new = News::find($id)->first();

        // show the edit form and pass the nerd
        return view('admin.news.edit')
            ->with('new', $new);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            // 'title' => 'integer',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the title
        if ($validator->fails()) {
            return Redirect::to('admin/news/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            // store
            $new = News::find($id);
            $new->title       = $request->get('title');
            $new->content      = $request->get('content');
            $new->save();

            // redirect
            Session::flash('message', 'Successfully updated new!');
            return Redirect::to('admin/news');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new = News::find($id)->first();
        $new->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the new!');
        return Redirect::to('admin/news');
    }
}
