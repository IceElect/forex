<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use App\Promo;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promo = Promo::orderBy('id', 'desc')->paginate(10);
        return view('admin.promo.index')->with('promo', $promo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'code' => 'integer',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the code
        if ($validator->fails()) {
            return Redirect::to('admin/promo/create')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            // store
            $promo = new Promo;
            $promo->code       = $request->get('code');
            $promo->discount      = $request->get('discount');
            $promo->discount_type    = $request->get('discount_type');
            $promo->date_start    = $request->get('date_start');
            $promo->date_end    = $request->get('date_end');
            $promo->count    = $request->get('count');
            $promo->uses_count    = 0;
            $promo->status    = $request->get('status');
            $promo->save();

            // redirect
            Session::flash('message', 'Successfully created promo!');
            return Redirect::to('admin/promo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promo = Promo::find($id);

        return view('admin.promo.show')
           ->with('promo', $promo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promo = Promo::find($id)->first();

        // show the edit form and pass the nerd
        return view('admin.promo.edit')
            ->with('promo', $promo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'code' => 'integer',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the code
        if ($validator->fails()) {
            return Redirect::to('admin/promo/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            // store
            $promo = Promo::find($id);
            $promo->code       = $request->get('code');
            $promo->discount      = $request->get('discount');
            $promo->discount_type    = $request->get('discount_type');
            $promo->date_start    = $request->get('date_start');
            $promo->date_end    = $request->get('date_end');
            $promo->count    = $request->get('count');
            $promo->status    = $request->get('status');
            $promo->save();

            // redirect
            Session::flash('message', 'Successfully updated promo!');
            return Redirect::to('admin/promo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $promo = Promo::find($id)->first();
        $promo->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the promo!');
        return Redirect::to('admin/promo');
    }

    function apply(Request $request) {
        $cart = (object) $request->session()->get('cart');
        if($request->get('code')){
            $code = $request->get('code');
            $promo = Promo::where(['code' => $code])->first();
            if($promo){
                $cart->promo = $promo;
                $request->session()->put('promo', (object) $promo);

                return response()->json(['response' => true]);
            }
        }

        $request->session()->put('promo', false);

        return response()->json(['response' => false]);
    }
}
