<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use App\Http\Myfxbook;

use App\Account;

use App\Signal;

class SignalsController extends Controller
{
    public function index()
    {
        $signals = Signal::orderBy('id', 'desc')->paginate(10);
        return view('admin.signals.index')->with([
            'signals' => $signals,
            'statuses' => [
                '<span class="text-muted">Waiting</span>',
                '<span class="text-success">Accepted</span>',
                '<span class="text-danger">Declined</span>',
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Signal  $signal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Signal $signal)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $signal = Signal::find($signal)->first();
        $signal->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // redirect
        Session::flash('message', 'Successfully deleted the signal!');
        return Redirect::to('admin/signals');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Signal $signal)
    {
        // show the edit form and pass the nerd
        return view('admin.signals.edit')
            ->with('signal', $signal);
    }

    function update(Request $request, Signal $signal) {

        $rules = array(
            // 'myfxbook_email' => 'required',
            // 'myfxbook_password' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        $data = [
            'status' => $request->status
        ];
        if($request->get('name')){
            $data['name'] = $request->get('name');
            $data['description'] = $request->get('description');
            $data['price'] = $request->get('price');
            $data['expiration_days'] = $request->get('expiration_days');
        }
        if($request->get('myfxbook_email')) {
            $token = Myfxbook::login($request->get('myfxbook_email'), $request->get('myfxbook_password'));

            $myfxbook_error = false;
            if(empty($token)) {
                $myfxbook_error = true;
            }else{
                $account = $signal->account;
                $accountInfo = Myfxbook::getAccountInfo($token, $account->login);
                $accountHistory = [];

                if(!$accountInfo){
                    $myfxbook_error = true;
                }else{
                    $accountHistory = Myfxbook::getAccountHistory($token, $accountInfo->id);
                }
            }

            if ($myfxbook_error) {
                if($myfxbook_error) {
                    $validator->errors()->add('myfxbook_email', 'Myfxbook account not found');
                    $validator->getMessageBag()->add('myfxbook_email', 'Myfxbook account not found');
                }
                return Redirect::to("admin/signals/{$signal->id}/edit/" )
                    ->withErrors($validator->errors())
                    ->withInput($request->all());
            }

            $account->setData($token, $accountInfo, $accountHistory);
        }
        $signal->update($data);
        return Redirect::to('admin/signals');
    }
}
