<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use App\Account;

class TasksController extends Controller
{
    function index() {
        $accounts = Account::whereRaw('(task_completed = 0 OR updated_at < NOW() - INTERVAL 15 DAY) AND online = 1')->orderBy('updated_at', 'asc')->paginate(30);
        // Account::whereRaw('updated_at > NOW() - INTERVAL 15 DAY')->orderBy('created_at', 'asc');
        return view('admin.tasks.index')->with([
            'accounts' => $accounts
        ]);
    }

    public function destroy($account_id)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Account::find($account_id)->update(['task_completed' => 1]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // redirect
        Session::flash('message', 'Successfully completed the task!');
        return Redirect::to('admin/tasks');
    }
}
