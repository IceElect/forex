<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use App\Signal;
use App\Account;
use App\Order;

use App\Http\Myfxbook;

use App\BrokerServer;

class ApiController extends Controller
{

    function __construct() {
        // set_error_handler(function($errno, $errstr, $errfile, $errline ){
        //     throw new \Exception($errstr, $errno, 0, $errfile, $errline);
        // });
    }

    function _response($data, $code = 200) {
        return response(['response' => true, 'error' => '', 'data' => $data], $code)
                ->header('Content-Type', 'application/json');

    }

    function _error($error, $code = 400) {
        return response(['response' => false, 'error' => $error], $code)
                ->header('Content-Type', 'application/json');

    }

    function top() {
        $signals = Signal::where('status', '=', 1)->paginate(9);
        return view('signals.top')->with('signals', $signals);
    }

    function account(Request $request) {
        $data = $request->all();

        $rules = [
            'account_id' => 'required',
            'server' => 'required',
            'last_order_id' => 'required',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->errors()->count()) {
            //TODO Handle your error
            return $this->_error($validator->errors()->first(), 400);
        }

        try{
            $server = BrokerServer::where('name', '=', $data['server'])->first();

            if(empty($server)){
                return $this->_error('Server Not Found', 404);
            }

            $account_id = $data['account_id'];
            $account = Account::where('login', '=', $account_id)->where('server_id', $server->id)->first();

            if(!empty($account)){
                foreach($account->subscribes as $key => $subscribe) {
                    $orders = Order::selectRaw('*, IF(expiration="1000-01-01 00:00:00", 0, expiration) as expiration, NOW() AS now')->where('signal_id', $subscribe->signal->id)
                                    ->where('id', '>', $data['last_order_id'])
                                    ->get();
                    // dd($subscribe->signal->id);
                    // dd($subscribe->signal->account_id);
                    $account->subscribes[$key]->orders = $orders;
                }
                return $this->_response($account);
            }else{
                return $this->_error('Account Not Found', 404);
            }
        }catch(Exception $e) {
            return $this->_error('Server Internal Error: ' . $e->getMessage(), 500);
        }
    }

    function order(){
        $data = json_decode(request()->getContent(), true);

        if(!$data) {
            return $this->_error("JSON Not Valid", 400);
        }

        $rules = [
            'account_id' => 'required',
            'action' => 'required',
            'ticket' => 'required',
            'symbol' => 'required',
            'type' => 'required',
            'volume' => 'required',
            'price' => 'required',
            'sl' => 'required',
            'tp' => 'required',
            'magic' => 'required',
            'expiration' => 'required',
            'balance' => 'required',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->errors()->count()) {
            //TODO Handle your error
            return $this->_error($validator->errors()->first(), 400);
        }

        try{
            $account_id = $data['account_id'];
            $account = Account::where('login', '=', $account_id)->first();

            if(!empty($account)){

                if(!empty($account->signal)){

                    $order = new Order;
                    $order->signal_id = $account->signal->id;
                    $order->action = $data['action'];
                    $order->ticket = $data['ticket'];
                    $order->symbol = $data['symbol'];
                    $order->type = $data['type'];
                    $order->volume = $data['volume'];
                    $order->price = $data['price'];
                    $order->sl = $data['sl'];
                    $order->tp = $data['tp'];
                    $order->magic = $data['magic'];
                    $order->expiration = ($data['expiration']) ? $data['expiration'] : "1000-01-01 00:00:00";
                    $order->comment = $data['comment'] ?? '';
                    $order->balance = $data['balance'];
                    $result = $order->save();

                    if($result){
                        return $this->_response($order);
                    }else{
                        return $this->_error('Database Error', 501);
                    }
                }else {
                    return $this->_error('Signal Not Found', 406);
                }
            }else{
                return $this->_error('Account Not Found', 404);
            }
        }catch(Exception $e) {
            return $this->_error('Server Internal Error: ' . $e->getMessage(), 500);
        }
    }
}
