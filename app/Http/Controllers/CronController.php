<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

use App\Http\Myfxbook;

use App\Signal;
use App\User;
use App\Account;

use App\Mail\ExprireNotify;

use App\Purchase;

class CronController extends Controller
{

    function notify () {
        $response = true;
        $users = User::get();
        $purchases = Purchase::get();

        foreach($purchases as $purchase) {
            if($purchase->account){
                Mail::to($purchase->account->user->email)->send(new ExprireNotify($purchase->account, $purchase->signal));
            }
        }
    }

    function fx() {

        $response = true;
        $accounts = Account::whereNotNull('myfxbook_session')->get();

        foreach($accounts as $account){
            $accountInfo = Myfxbook::getAccountInfo($account->myfxbook_session, $account->login);

            $accountData = empty($account->data) ? new AccountData() : $account->data;
            dd($accountInfo);
            $accountHistory = Myfxbook::getAccountHistory($account->myfxbook_session, $accountInfo->id);
            // dd($accountInfo);

            $accountData->account_id = $account->id;
            $accountData->acc_id = $accountInfo->id;
            $accountData->growth = $accountInfo->gain;
            $accountData->profit = $accountInfo->profit;
            $accountData->profit_factor = $accountInfo->profitFactor;
            $accountData->balance = $accountInfo->balance;
            $accountData->drawdown = $accountInfo->drawdown;
            $accountData->pips = $accountInfo->pips;
            $accountData->monthly = $accountInfo->monthly;
            $accountData->daily = $accountInfo->daily;
            $accountData->creation_date = date('Y-m-d H:i:s', strtotime($accountInfo->creationDate));
            $accountData->trades_count = count($accountHistory);

            $accountData->server = $accountInfo->server->name;
            $accountData->is_demo = $accountInfo->demo;
            $accountData->equity = $accountInfo->equity;
            $accountData->deposits = $accountInfo->deposits;
            $accountData->withdrawals = $accountInfo->withdrawals;

            if(!$accountData->save()){
                $response = false;
                dd('Failed save data for <b>account ' . $account->id . '</b>', true);
            }
        }

        if($response)
            return Redirect::to('catalog');
    }
}
