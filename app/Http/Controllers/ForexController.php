<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Subscribe;
use App\User;
use App\Signal;
use App\Account;
use App\Purchase;

use App\Http\ReferralHelper;

use Carbon\Carbon;

// use Srmklive\PayPal\Services\ExpressCheckout;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\PayerInfo;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;

class ForexController extends Controller
{

    private $_api_context;

    function __construct(){
        $this->middleware('auth');

        // 123123123123
        $paypal_conf = config('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
        // dd(Payment::all(['count' => 10]));
    }

    function services() {
        $signals = Auth::user()->expiredSignals();
        $onlines = Auth::user()->expiredOnlines();
        $not_onlines = Auth::user()->notexpiredOnlines();

        return view('services')->with([
            'expiredSignals' => $signals,
            'expiredOnlines' => $onlines,
            'notexpiredOnlines' => $not_onlines,
        ]);
    }

    function balance_history(){
        // dump(Auth::user()->transactions);
        return view('balance')->with([
            'transactions' => Auth::user()->transactions()->orderBy('created_at', 'desc')->get(),
            'statuses' => [
                '<span class="text-muted">Waiting...</span>',
                '<span class="text-success">Accepted</span>',
                '<span class="text-danger">Declined</span>',
            ],
        ]);
    }

    function accounts(){

        $accounts = Auth::user()->usedAccounts;
        $expiredSignals = Auth::user()->expiredSignals();
        $deactivatedSignals = Auth::user()->deactivatedSignals();

        // dd($diactivatedSignals);
        // $purshaces = Auth::user()->purshaces->leftJoin('signals');
        // dd($purshaces);

        // Auth::user()->deposit(10);

        return view('signals')->with([
            'is_edit' => 0,
            'accounts' => $accounts,
            'expiredSignals' => $expiredSignals,
            'deactivatedSignals' => $deactivatedSignals
        ]);
    }

    function signals_activate(Request $request){
        $purchasesIds = (array) $request->get('purchases');
        $purchases = Purchase::whereIn('id', $purchasesIds)->get();
        $temp = [];

        foreach($purchases as $key => $item) {
            if(in_array($item->signal_id, $temp)){
                unset($purchases[$key]);
            }else{
                $temp[] = $item->signal_id;
            }
        }

        @$percent = round( 100 / count($temp) );

        if($request->get('account_id')){
            foreach($purchasesIds as $purchase_id => $percent){
                $purchase = Purchase::find($purchase_id);
                $purchase->percent = $percent;
                $purchase->activated_at = date('Y-m-d H:i:s');
                // $subscribe->master_id = $purchase_id;
                $purchase->account_id = $request->get('account_id');
                $purchase->save();
            }
            return Redirect::to('accounts/list');
        }else{
            $accounts = Auth::user()->unusedAccounts;
            // dd($accounts);

            return view('signals_activate')->with([
                'purchases' => $purchases,
                'percent' => $percent,
                'accounts' => $accounts,
            ]);
        }
    }

    function signals_delete(Request $request){
        $purchase_id = $request->get('purchase_id');

        $purchase = Purchase::find($purchase_id);
        $account = $purchase->account;

        $purchase->percent = NULL;
        $purchase->account_id = NULL;
        $result = $purchase->save();
        if($result){
            // dd($account);
            $subscribeIds = $account->subscribes->mapWithKeys(function ($item) {
                return [$item['signal_id'] => $item['id']];
            });

            $subscribeIds = array_keys($subscribeIds->all());

            $unusedSignals = Auth::user()->deactivatedSignals()->whereNotIn('signal_id', $subscribeIds);
            $deactivatedSignals = Auth::user()->deactivatedSignals();

            $is_edit = (int) $request->get('is_edit');

            $data = [
                'account' => $account,
                'is_edit' => $is_edit,
                'unusedSignals' => $unusedSignals,
                'deactivatedSignals' => $deactivatedSignals,
            ];

            $accountHtml = view('signals.account')->with($data)->render();
            $deactivatedHtml = view('signals.deactivated')->with($data)->render();
            return response()->json([
                'response' => true,
                'accountHtml' => $accountHtml,
                'deactivatedHtml' => $deactivatedHtml
            ]);
        }
    }

    function signals_account(Request $request){
        $is_edit = ($request->get('is_edit') == "true") ? 1 : 0;
        $account_id = $request->get('account_id');
        $account = Account::find($account_id);

        if($request->get('purchase_id')){
            $purchase = Purchase::find($request->get('purchase_id'));
            $purchase->percent = 0;
            $purchase->activated_at = date('Y-m-d H:i:s');
            $purchase->account_id = $account_id;
            $purchase->save();
        }

        if(!$is_edit){
            $signals = (array) $request->get('signals');
            foreach($signals as $purchase_id => $percent){
                $purchase = Purchase::find($purchase_id);
                $purchase->percent = $percent;
                $purchase->save();
            }
        }

        // dd($account->subscribes->items);

        $subscribeIds = $account->subscribes->mapWithKeys(function ($item) {
            return [$item['signal_id'] => $item['id']];
        });

        $subscribeIds = array_keys($subscribeIds->all());

        $unusedSignals = Auth::user()->deactivatedSignals()->whereNotIn('signal_id', $subscribeIds);
        $deactivatedSignals = Auth::user()->deactivatedSignals();
        // dd($deactivatedSignals->count());

        $data = [
            'account' => $account,
            'is_edit' => $is_edit,
            'unusedSignals' => $unusedSignals,
            'deactivatedSignals' => $deactivatedSignals,
        ];

        $accountHtml = view('signals.account')->with($data)->render();
        $deactivatedHtml = view('signals.deactivated')->with($data)->render();
        return response()->json([
            'response' => true,
            'accountHtml' => $accountHtml,
            'deactivatedHtml' => $deactivatedHtml
        ]);
    }

    function signals_prolong(Request $request){
        $total = 0;

        $signals = array();
        $purchases = $request->get('purchases');
        if($purchases){
            foreach($purchases as $purchase_id){
                $purchase = Purchase::find($purchase_id);
                $signals[$purchase_id] = $purchase->signal;
                $signals[$purchase_id]->purchase_id = $purchase_id;
            }
        }

        foreach($signals as $signal){
            $signal->quantity = 1;
            $total += $signal->price;
        }

        $request->session()->put('cart', (object) array(
            'total' => $total,
            'items' => $signals,
            'method' => 'prolong',
            'promo' => false,
        ));
        return Redirect::to('cart');

        if(Auth::user()->canWithdraw($total)){
            foreach($signals as $purchase_id => $signal){
                $this->_buy($signal, $purchase_id);
            }
            ReferralHelper::bonus($total);
        }

        return Redirect::to('accounts');
    }

    function reward(Request $request, $signal_id = false) {
        if($signal_id){
            $signal = Signal::find($signal_id);
            $signal->quantity = 1;
            $signal->is_reward = 1;

            $reward = Auth::user()->rewards()->first();
            $reward->status = 1;
            $reward->save();

            $this->_buy($signal, false);
            ReferralHelper::bonus($signal->price);
            return Redirect::to('accounts');
        }
        return $this->catalog($request, true);
    }

    function catalog(Request $request, $reward = false){

        $sort = $request->get('sort');
        $direction = $request->get('direction');

        if($request->get('sort') == 'age'){
            $sort = 'creation_date';
            $direction = ($request->get('direction') == 'asc') ? 'desc' : 'asc';
        }

        $signals = ($request->get('sort')) ? Signal::orderBy($sort, $direction)->groupBy('signals.id') : Signal::groupBy('signals.id');
        $signals = $signals
            ->leftJoin('signal_user', 'signal_user.signal_id', '=', 'signals.id')
            ->leftJoin('account_data', 'signals.account_id', '=', 'account_data.account_id')
            ->whereRaw('account_data.growth IS NOT NULL')
            ->where('signals.status', '=', '1')
            ->select(
                'signals.*',
                'account_data.growth',
                'account_data.pips',
                'account_data.drawdown',
                'account_data.profit_factor',
                'account_data.monthly',
                'account_data.daily',
                'account_data.trades_count',
                DB::raw('COUNT(signal_user.id) as subscribes_count')
                )
            ->paginate(15);
        // $signals = Signal::get();

        return view('catalog')->with(['signals' => $signals, 'is_reward' => $reward]);
    }

    function catalog_pay(Request $request){
        $signalsIds = (array) $request->get('signals');
        $signals = Signal::whereIn('id', $signalsIds)->get();

        // foreach($signals as $signal){
        //     $signal->quantity = 1;
        //     $total += $signal->price;
        // }

        $cart = $this->_add($signals);
        $cart = $this->_calcTotal($cart);
        // $cart = $this->_calcDiscount($cart);
        // dd($cart);

        // $old_data = array(
        //     'total' => $total,
        //     'items' => $signals,
        //     'method' => 'buy',
        //     'promo' => false,
        // );

        $request->session()->put('cart', (object) $cart);
        return Redirect::to('cart');

        if(Auth::user()->canWithdraw($total)){
            foreach($signals as $signal){
                $this->_buy($signal);
            }
            ReferralHelper::bonus($total);
        }

        return Redirect::to('accounts');
    }

    function cart(Request $request) {
        $cart = (object) $request->session()->get('cart');
        if(empty($cart->items))
            return Redirect::to('');

        $cart = $this->_calcDiscount($cart);

        $data = [
            'total' => $cart->total,
            'signals' => $cart->items,
            'method' => $cart->method,
            'discount' => $cart->discount,
            'promo' => $cart->promo,
        ];

        return view('cart')->with($data);
    }

    function cart_update(Request $request) {
        $cart = (object) $request->session()->get('cart');

        $response = false;

        $action = $request->get('action');
        $signal_id = $request->get('signal_id');
        $purchase_id = $request->get('purchase_id');

        foreach($cart->items as $key => $item){
            if(
                $purchase_id == $item->purchase_id &&
                $signal_id == $item->id
            ) {
                switch($action){
                    case 'quantity':
                        $cart->items[$key]->quantity = $request->get('quantity');
                        $response = true;
                    break;
                    case 'remove':
                        unset($cart->items[$key]);
                        $response = true;
                    break;
                }
            }
        }

        $cart = $this->_calcTotal($cart);
        $request->session()->put('cart', (object) $cart);
        $cart = $this->_calcDiscount($cart);

        return response()->json($cart);
    }

    function cart_buy(Request $request){
        $cart = (object) $request->session()->get('cart');
        $cart = $this->_calcTotal($cart);
        $cart = $this->_calcDiscount($cart);

        if(Auth::user()->canWithdraw($cart->total)){
            $signals_list = [];
            foreach($cart->items as $signal){
                $purchase_id = @$signal->purchase_id;
                $this->_buy($signal, $purchase_id);

                $signals_list[] = "<a href='" . route('admin.signals.edit', ['signal' => $signal->id], false) . "'>" . $signal->name . " </a> x" . ($signal->quantity ?? 1 ) ;
            }

            ReferralHelper::bonus($cart->total);

            $comission = (int) env('COMISSION_PERCENT');
            $comission = $cart->total / 100 * $comission;

            @Auth::user()->forceWithdraw($cart->total, ['description' => ucfirst($cart->method) . ' Signals', 'signals' => implode(', ', $signals_list), 'comission' => $comission])->update(['type' => 'buy']);

            $request->session()->forget('cart');
            $request->session()->forget('promo');
            return Redirect::to('accounts/list');
        }else{
            return Redirect::to('cart');
        }
    }

    function _add($items) {
        $cart = session()->get('cart');

        if(empty($cart) || $cart->method == 'prolong'){
            $cart = (object) array(
                'total' => 0,
                'items' => array(),
                'method' => 'buy',
                'promo' => false,
            );
        }else{
            $cart = (object) $cart;
        }

        foreach ($items as $key => $signal) {
            if(isset($cart->items[$signal->id])){
                $cart->items[$signal->id]->quantity++;
            }else{
                $signal->quantity = 1;
                $cart->items[$signal->id] = $signal;
            }
            $cart->total += $signal->price;
        }

        return $cart;
    }

    function _calcTotal($cart) {
        $cart->total = 0;
        foreach($cart->items as $signal){

            if(!isset($signal->quantity))
                $signal->quantity = 1;

            $cart->total += $signal->price * $signal->quantity;
        }

        return $cart;
    }

    function _calcDiscount($cart) {
        $promo = session()->get('promo');

        if($promo) {
            $cart->promo = $promo;
            if($promo['discount_type'] == 0){
                $cart->discount = $promo['discount'];
            }else{
                $cart->discount = $cart->total / 100 * $promo['discount'];
            }
            $cart->discount = round($cart->discount);
            $cart->total = $cart->total - $cart->discount;
            // $cart->solo_discount = $cart->discount / count($cart->items);
        }else{
            $cart->discount = false;
        }

        return $cart;
    }

    function _buy($signal, $purchase_id = false){
        for($i = 0; $i < $signal->quantity; $i++){
            if($purchase_id){ // Prolong
                $purchase = Purchase::find($purchase_id);

                $purchase_ex = strtotime($purchase->expiredDate());
                if(time() > $purchase_ex) $purchase_ex = time();

                $purchase->activated_at = date('Y-m-d H:i:s', $purchase_ex);
            }else{ // Buy
                $purchase = new Purchase();
            }
            $purchase->user_id = Auth::user()->id;
            $purchase->signal_id = $signal->id;
            $purchase->is_reward = (int) @$signal->is_reward;
            $result = $purchase->save();

            if($result) { // Author pay
                Signal::where('id', '=', $purchase->signal_id)->increment('buys_count', 1);

                $comission = (int) env('COMISSION_PERCENT');
                $earned = $signal->price / 100 * ( 100 - $comission );
                $signal->user->deposit($earned, 'earned', [
                    'description' => 'Earned from selling a signal ":signal_name" by user :user_id',
                    'signal_id' => $signal->id,
                    'signal_name' => $signal->name,
                    'user_id' => Auth::user()->id
                ]);
            }
        }
    }

    function catalog_calc(Request $request){

        $total = 0;

        if($request->get('prolong')){
            $signals = array();
            $purchases = $request->get('purchases');
            if($purchases){
                foreach($purchases as $purchase_id){
                    $purchase = Purchase::find($purchase_id);
                    $signals[] = $purchase->signal;
                }
            }
        }else{
            $signalsIds = (array) $request->get('signals');
            $signals = Signal::whereIn('id', $signalsIds)->get();
        }

        foreach($signals as $signal){
            $total += $signal->price;
        }

        return response()->json(['response' => ($total <= Auth::user()->balance), 'total' => $total]);
    }
}
