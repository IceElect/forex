<?php

namespace App\Http\Controllers;

use App\Http\ReferralHelper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\PayerInfo;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use URL;

class PaymentController extends Controller
{
    public function __construct()
    {
         /** PayPal api context **/
         $paypal_conf = config('paypal');
         $this->_api_context = new ApiContext(new OAuthTokenCredential(
             $paypal_conf['client_id'],
             $paypal_conf['secret'])
         );
         $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithpaypal(Request $request)
    {
      $amountToBePaid = $request->get('amountToBePaid');
      $payer = new Payer();
      $payer->setPaymentMethod('paypal');

      $item_1 = new Item();
      $item_1->setName(__('User Balance') . ' ' . Auth::user()->id ) /** название элемента **/
              ->setCurrency('USD')
              ->setQuantity(1)
              ->setPrice($amountToBePaid); /** цена **/

      $item_list = new ItemList();
      $item_list->setItems(array($item_1));

      $amount = new Amount();
      $amount->setCurrency('USD')
             ->setTotal($amountToBePaid);

      $redirect_urls = new RedirectUrls();
      /** Укажите обратный URL **/
      $redirect_urls->setReturnUrl(URL::route('status'))
                ->setCancelUrl(URL::route('status'));

      $transaction = new Transaction();
      $transaction->setAmount($amount)
              ->setItemList($item_list)
              ->setDescription('Описание транзакции');

      $payment = new Payment();
      $payment->setIntent('Sale')
              ->setPayer($payer)
              ->setRedirectUrls($redirect_urls)
              ->setTransactions(array($transaction));
      try {
           $payment->create($this->_api_context);
      } catch (\PayPal\Exception\PPConnectionException $ex) {
           if (\Config::get('app.debug')) {
              \Session::put('error', 'Таймаут соединения');
              return Redirect::route('/');
           } else {
              \Session::put('error', 'Возникла ошибка, извините за неудобство');
              return Redirect::route('/');
           }
      }

      foreach ($payment->getLinks() as $link) {
        if ($link->getRel() == 'approval_url') {
           $redirect_url = $link->getHref();
           break;
        }
      }

      /** добавляем ID платежа в сессию **/
      \Session::put('paypal_payment_id', $payment->getId());

      if (isset($redirect_url)) {
         /** редиректим в paypal **/
         return Redirect::away($redirect_url);
      }

      \Session::put('error', 'Произошла неизвестная ошибка');
      return Redirect::route('/');
    }

    public function getPaymentStatus(Request $request)
    {
      /** Получаем ID платежа до очистки сессии **/
      $payment_id = Session::get('paypal_payment_id');
      if(!$payment_id)
        $payment_id = $request->get('paymentId');
      /** Очищаем ID платежа **/
      Session::forget('paypal_payment_id');

      if (empty($request->PayerID) || empty($request->token)) {
         session()->flash('error', 'Payment failed');
         return Redirect::route('/');
      }

      $payment = Payment::get($payment_id, $this->_api_context);
      $execution = new PaymentExecution();
      $execution->setPayerId($request->PayerID);

      /** Выполняем платёж **/
      try{
          $result = $payment->execute($execution, $this->_api_context);
      }catch(PayPalConnectionException $e){
          dd($e->getBody());
      }

      if ($result->getState() == 'approved') {
         session()->flash('success', 'Платеж прошел успешно');
         // dd($result);
         // dd($result->transactions[0]->item_list->items[0]);
         $sum = $result->transactions[0]->item_list->items[0]->price;
         $name = $result->transactions[0]->item_list->items[0]->name;
         // $user_id = explode('#', $name)[1];
         // ReferralHelper::bonus($sum);
         Auth::user()->deposit($sum, 'paypal', ['description' => 'Deposit with Paypal']);
         ReferralHelper::reward_event('replenishment');
         return Redirect::route('cart');
      }

      session()->flash('error', 'Платеж не прошел');
      return Redirect::route('/');
    }

    function withdraw(Request $request) {
        if($request->get('sum')) {
            $transaction = @Auth::user()->withdraw($request->get('sum'), [
                'description' => __('Withdraw :sum$', ['sum' => $request->get('sum')]),
                'requisites' => \nl2br($request->get('requisites'))
            ], false);

            if($transaction) {
                $message = __('<b>Your request has been successfully sent.</b> You can track the status of your request on the <a href="/balance">balance history page</a>. Approval takes up to 3 days.');
                Session::flash('message', $message);
                return Redirect::to(route('home'))->with('message', $message);
            }
        }
        return view('payment.withdraw')->with([

        ]);
    }
}
