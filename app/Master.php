<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    public $table = 'masters';
}
