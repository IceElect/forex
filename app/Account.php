<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Account extends Model
{

    public $table = 'accounts';
    protected $guarded = [];

    protected $with = array('subscribes');

    function signal() {
        return $this->hasOne('App\Signal');
    }

    function broker_server(){
        // dd($this->belongsTo('App\BrokerServer', 'server_id'));
        return $this->belongsTo('App\BrokerServer', 'server_id');
    }

    function isExpired() {
        return ($this->updated_at->addDays(15)->timestamp < time());
    }

    function subscribes(){
        $query = $this->hasMany('App\Purchase', 'account_id')
            ->select('signal_user.*')
            ->leftJoin('signals', 'signals.id', '=', 'signal_user.signal_id')
            ->whereRaw('IF(signal_user.is_reward = 0, signal_user.activated_at > NOW() - INTERVAL signals.expiration_days DAY, signal_user.activated_at > NOW() - INTERVAL 7 DAY) AND signal_user.account_id IS NOT NULL');
            // ->whereNotNull('account_id');
        // dd($query->get());
        // dd($query->toSql());
        return $query;
    }

    function subscribes_old(){
        return $this->hasMany('App\Subscribe', 'client_id');
    }

    function user(){
        return $this->belongsTo('App\User');
    }

    function data(){
        return $this->hasOne('App\AccountData');
    }

    function setData($session, $accountInfo, $accountHistory) {

        $this->update([
            'myfxbook_session' => $session
        ]);

        $accountData = (object) array();
        $accountData->account_id = $this->id;
        $accountData->acc_id = $accountInfo->id;
        $accountData->growth = $accountInfo->gain;
        $accountData->profit = $accountInfo->profit;
        $accountData->profit_factor = $accountInfo->profitFactor;
        $accountData->balance = $accountInfo->balance;
        $accountData->drawdown = $accountInfo->drawdown;
        $accountData->pips = $accountInfo->pips;
        $accountData->monthly = $accountInfo->monthly;
        $accountData->daily = $accountInfo->daily;
        $accountData->creation_date = date('Y-m-d H:i:s', strtotime($accountInfo->creationDate));
        $accountData->trades_count = count($accountHistory);
        $accountData->server = $accountInfo->server->name;
        $accountData->is_demo = $accountInfo->demo;
        $accountData->equity = $accountInfo->equity;
        $accountData->deposits = $accountInfo->deposits;
        $accountData->withdrawals = $accountInfo->withdrawals;
        $accountData = (array) $accountData;

        if(!$this->data) {
            // dd($accountData);
            return $this->data()->create($accountData);
        }else {
            return $this->data->update($accountData);
        }
    }

    function chart($width = 200, $linet = 0){
        if(!$this->myfxbook_session) {
            return '';
        }
        $session = $this->myfxbook_session;
        $account_id = $this->data->acc_id;
        // $account_id = $this->login;

        $height = $width * 0.75;
        if($height > 300) {
            $height = 300;
        }

        $gridColor = ($linet) ? 'ffffff' : 'BDBDBD';

        $url = "https://widgets.myfxbook.com/api/get-custom-widget.png?session={$session}&id={$account_id}&width={$width}&height={$height}&bart=0&linet={$linet}&bgColor=ffffff&chartbgc=ffffff&gridColor={$gridColor}&lineColor=00CB05&barColor=FF8D0A&fontColor=000000&title=&titles=20&chartbgc=474747";
        return $url;
    }

    function percentSum(){
        $sum = 0;
        foreach($this->subscribes as $subscribe){
            $sum += $subscribe->percent;
        }
        return $sum;
    }

    function expiredDate() {
        return $this->updated_at->addDays(env('ONLINE_EXPIRED_DAYS', 15));
    }
}
