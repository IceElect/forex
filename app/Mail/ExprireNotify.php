<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Signal;
use App\User;
use App\Account;

class ExprireNotify extends Mailable
{
    use Queueable, SerializesModels;

    public $signal;
    public $account;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    // public function __construct(Signal $signal)
    public function __construct(Account $account, Signal $signal)
    {
        $this->signal = $signal;
        $this->account = $account;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mails.expireNotify')
                    ->subject( __('Your Signal Expires in 5 days') );
    }
}
