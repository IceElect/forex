<?php

namespace App\Listeners;

// use App\Events\OrderShipped;

use Illuminate\Auth\Events\Verified;

use App\Http\ReferralHelper;

class RewardVerifiedUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderShipped  $event
     * @return void
     */
    public function handle(Verified $event)
    {
        ReferralHelper::reward_event('registered');
        // Access the order using $event->order...
    }
}
