<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasRolesAndPermissions;

class Role extends Model
{
    use HasRolesAndPermissions;

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'roles_permissions');
    }
}
