<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
    public $table = 'subscribes';

    function master(){
        return $this->belongsTo('App\Account', 'master_id');
    }

    function client(){
        return $this->belongsTo('App\Account', 'client_id');
    }

    function signal(){
        return $this->master->hasOne('App\Signal', 'account_id');
    }
}
