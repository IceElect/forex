<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public $table = 'signal_user';

    protected $fillable = ['percent'];

    protected $dates = ['created_at', 'updated_at', 'activated_at'];

    private $column = false;

    private $direction = false;

    function user(){
        return $this->account->belongsTo('App\User');
    }

    function signal(){
        return $this->belongsTo('App\Signal');
    }

    function account(){
        return $this->belongsTo('App\Account');
    }

    function expiredDate(){
        if(!empty($this->activated_at)){
            $expiration_days = (!$this->is_reward) ? $this->signal->expiration_days : 7;
            return $this->activated_at->addDays($expiration_days);
        }else{
            return $this->created_at->addDays(15);
        }
    }
}
