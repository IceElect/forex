<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Signal extends Model
{
    use Sortable;

    private $column;
    private $direction;
    protected $guarded = [];

    public function __construct($column = 'signals.id', $direction = 'asc')
    {
        $this->column = $column;
        $this->direction = $direction;
    }

    function growth() {
        return $this->account->data->growth;
    }

    function account(){
        return $this->belongsTo('App\Account');
    }

    function subscribes(){
        return $this->hasMany('App\Purchase');
    }

    function subscribes_count() {
        return $this->subscribes->count();
    }

    function user(){
        return $this->account->belongsTo('App\User');
    }

    function marks(){
        // <!-- Rank #422, Real, USD, Forex Cheaf -->
        $marks = array();

        $marks[] = $this->is_demo ? 'Demo' : 'Real';
        $marks[] = strtoupper($this->account->currency);
        $marks[] = $this->account->data->server;

        return implode(', ', $marks);
    }
}
