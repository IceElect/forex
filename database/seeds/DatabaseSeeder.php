<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);

        DB::table('accounts')->insert([
            'user_id' => '1',
            'login' => '1494850',
            'server' => '123',
            'currency' => 'rub',
            'myfxbook_session' => 'lcZXl2EJUmEqPI2RBD6a543814',
        ]);
        DB::table('accounts')->insert([
            'user_id' => '2',
            'login' => '29694146',
            'server' => '1234',
            'currency' => 'rub',
            'myfxbook_session' => 'lcZXl2EJUmEqPI2RBD6a543814',
        ]);

        DB::table('accounts')->insert([
            'user_id' => '3',
            'login' => '29879218',
            'server' => '12345',
            'currency' => 'rub',
            'myfxbook_session' => 'lcZXl2EJUmEqPI2RBD6a543814',
        ]);

        DB::table('signals')->insert([
            'account_id' => '1',
            'price' => '950',
            'name' => 'Test Signal',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel quibusdam, magni aspernatur accusantium! Consequuntur eveniet laborum nostrum impedit aspernatur sed et vel repudiandae dolor possimus? Quos eos voluptate dolore totam dolores! Voluptas corporis consequuntur quae quisquam dicta voluptatem velit neque. Cum laboriosam rem rerum, quaerat maiores explicabo quia ipsa error, animi molestias incidunt a soluta dolore delectus quibusdam blanditiis exercitationem corporis necessitatibus, nobis nostrum. Officiis totam aspernatur laudantium inventore ratione nobis voluptatem in porro dolore omnis error recusandae nulla, fugit praesentium velit, rem doloribus qui sapiente voluptatum expedita, saepe officia minima nihil quaerat iure. Commodi suscipit dolore, vero eos! Ea!'
        ]);

        DB::table('signals')->insert([
            'account_id' => '2',
            'price' => '50',
            'name' => 'Good Signal',
            'expiration_days' => 60,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel quibusdam, magni aspernatur accusantium! Consequuntur eveniet laborum nostrum impedit aspernatur sed et vel repudiandae dolor possimus? Quos eos voluptate dolore totam dolores! Voluptas corporis consequuntur quae quisquam dicta voluptatem velit neque. Cum laboriosam rem rerum, quaerat maiores explicabo quia ipsa error, animi molestias incidunt a soluta dolore delectus quibusdam blanditiis exercitationem corporis necessitatibus, nobis nostrum. Officiis totam aspernatur laudantium inventore ratione nobis voluptatem in porro dolore omnis error recusandae nulla, fugit praesentium velit, rem doloribus qui sapiente voluptatum expedita, saepe officia minima nihil quaerat iure. Commodi suscipit dolore, vero eos! Ea!'
        ]);
        DB::table('signals')->insert([
            'account_id' => '3',
            'price' => '10',
            'name' => 'Very Good Signal',
            'expiration_days' => 90,
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel quibusdam, magni aspernatur accusantium! Consequuntur eveniet laborum nostrum impedit aspernatur sed et vel repudiandae dolor possimus? Quos eos voluptate dolore totam dolores! Voluptas corporis consequuntur quae quisquam dicta voluptatem velit neque. Cum laboriosam rem rerum, quaerat maiores explicabo quia ipsa error, animi molestias incidunt a soluta dolore delectus quibusdam blanditiis exercitationem corporis necessitatibus, nobis nostrum. Officiis totam aspernatur laudantium inventore ratione nobis voluptatem in porro dolore omnis error recusandae nulla, fugit praesentium velit, rem doloribus qui sapiente voluptatum expedita, saepe officia minima nihil quaerat iure. Commodi suscipit dolore, vero eos! Ea!'
        ]);

        DB::table('option')->insert([
            'key' => 'hello',
            'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel quibusdam, magni aspernatur accusantium! Consequuntur eveniet laborum nostrum impedit aspernatur sed et vel repudiandae dolor possimus? Quos eos voluptate dolore totam dolores! Voluptas corporis consequuntur quae quisquam dicta voluptatem velit neque. Cum laboriosam rem rerum, quaerat maiores explicabo quia ipsa error, animi molestias incidunt a soluta dolore delectus quibusdam blanditiis exercitationem corporis necessitatibus, nobis nostrum. Officiis totam aspernatur laudantium inventore ratione nobis voluptatem in porro dolore omnis error recusandae nulla, fugit praesentium velit, rem doloribus qui sapiente voluptatum expedita, saepe officia minima nihil quaerat iure. Commodi suscipit dolore, vero eos! Ea!</p>',
        ]);

        // DB::table('signal_user')->insert([
        //     'user_id' => 1,
        //     'signal_id' => 1,
        //     'created_at' => date('Y-m-d H:i:s'),
        //     'updated_at' => date('Y-m-d H:i:s'),
        // ]);

        // DB::table('subscribes')->insert([
        //     'client_id' => 1,
        //     'master_id' => 2,
        //     'percent' => 50,
        //     'created_at' => date('Y-m-d H:i:s'),
        //     'updated_at' => date('Y-m-d H:i:s'),
        // ]);
    }
}
