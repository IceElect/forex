<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $createTasks = Permission::where('slug','create-tasks')->first();
        $manageUsers = Permission::where('slug','manage-users')->first();

        $manager = new Role();
        $manager->name = 'Admin';
        $manager->slug = 'admin';
        $manager->save();

        $manager->givePermissionsTo('manage-users');
        $manager->givePermissionsTo('create-tasks');

        $developer = new Role();
        $developer->name = 'Developer';
        $developer->slug = 'developer';
        $developer->save();

        $developer->givePermissionsTo('manage-users');
        $developer->givePermissionsTo('create-tasks');
    }
}
