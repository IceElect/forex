<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;
use App\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = Role::where('slug', 'admin')->first();
        $developer = Role::where('slug','developer')->first();

        $user1 = new User();
        $user1->name = 'Elect';
        $user1->email = 'a@slto.ru';
        $user1->password = Hash::make('Admin333');
        $user1->save();
        $user1->roles()->attach($developer);
        $user1->deposit(1000);

        $user2 = new User();
        $user2->name = 'Elect2';
        $user2->email = 'a2@slto.ru';
        $user2->password = Hash::make('Admin333');
        $user2->save();
        $user2->roles()->attach($admin);
        $user2->deposit(1000);

        $user3 = new User();
        $user3->name = 'Elect3';
        $user3->email = 'a3@slto.ru';
        $user3->password = Hash::make('Admin333');
        $user3->save();
        $user3->deposit(1000);
    }
}
