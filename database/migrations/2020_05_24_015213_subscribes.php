<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Subscribes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        return;
        Schema::create('subscribes', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->foreignId('purchase_id')->constrained('signal_user');
            // $table->foreignId('master_id')->constrained('accounts');
            // $table->foreignId('client_id')->constrained('accounts');
            $table->integer('percent');
            $table->timestamps();
            $table->primary(['id']);
            // $table->primary(['master_id','client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribes');
    }
}
