<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AccountsOnlineFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->char('password', 128)->default('')->nullable();
            $table->boolean('broadcast')->default(0);
            $table->boolean('task_completed')->default(0);
            $table->string('login', 128)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropColumn('password');
            $table->dropColumn('broadcast');
            $table->dropColumn('task_completed');
            // $table->bigInteger('login')->change();
        });
    }
}
