<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Purchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signal_user', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('signal_id')->constrained('signals');
            $table->foreignId('account_id')->nullable()->constrained('accounts');
            $table->integer('percent')->nullable();
            $table->boolean('is_reward')->default(0);
            $table->timestamp('activated_at')->nullable();
            $table->timestamps();
            // $table->primary(['user_id', 'signal_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signal_user');
    }
}
