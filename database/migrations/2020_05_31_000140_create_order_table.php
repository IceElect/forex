<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id')->constrained('accounts');
            $table->integer('action');
            $table->bigInteger('ticket');
            $table->char('symbol', 40);
            $table->integer('type');
            $table->double('volume');
            $table->double('price');
            $table->double('sl');
            $table->double('tp');
            $table->integer('magic');
            $table->datetime('expiration');
            $table->text('comment')->nullable();
            $table->double('balance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
