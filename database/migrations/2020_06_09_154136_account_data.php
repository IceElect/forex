<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AccountData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_data', function (Blueprint $table) {
            // $table->id()->autoIncrement();
            $table->foreignId('account_id')->constrained('accounts');
            $table->integer('acc_id');
            $table->float('pips');
            $table->float('daily');
            $table->float('monthly');
            $table->float('growth');
            $table->float('balance');
            $table->float('drawdown');
            $table->float('profit');
            $table->float('profit_factor');

            $table->float('equity');
            $table->float('deposits');
            $table->float('withdrawals');
            $table->char('server', 80);
            $table->boolean('is_demo');

            $table->integer('trades_count');
            $table->dateTime('creation_date');
            $table->timestamps();

            $table->primary(['account_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
