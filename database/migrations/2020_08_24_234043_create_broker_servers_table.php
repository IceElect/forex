<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\BrokerServer;

class CreateBrokerServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broker_servers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('broker_id')->default('1')->constrained('brokers');
            $table->char('name', 128);
            $table->timestamps();
        });

        BrokerServer::create(['name' => 'First Server', 'broker_id' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broker_servers');
    }
}
