<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Accounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->foreignId('user_id')->constrained();
            $table->bigInteger('login');
            $table->char('server', 80);
            $table->char('currency', 40);
            $table->char('myfxbook_session', 40)->nullable();
            $table->boolean('online')->default(0);
            $table->timestamps();
            $table->unique(['login','server']);
        });

        // Schema::table('account', function (Blueprint $table) {
        //     $table->unsignedBigInteger('user_id');
        //
        //     $table->foreign('user_id')
        //         ->references('name')
        //         ->on('users')
        //         ->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
