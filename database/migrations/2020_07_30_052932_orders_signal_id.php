<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrdersSignalId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('order', 'account_id'))
        {
            Schema::table('order', function (Blueprint $table) {
                $table->dropForeign(['account_id']);
                $table->dropColumn('account_id');
                $table->foreignId('signal_id')->after('id')->constrained('signals');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('order', 'signal_id'))
        {
            Schema::table('order', function (Blueprint $table) {
                $table->dropForeign(['signal_id']);
                $table->dropColumn('signal_id');
                $table->foreignId('account_id')->after('id')->constrained('accounts');
            });
        }
    }
}
