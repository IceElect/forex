<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SignalCounters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('signals', 'buys_count'))
        {
            Schema::table('signals', function (Blueprint $table) {
                $table->integer('buys_count')->after('status')->default(0);
                $table->integer('views_count')->after('status')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('buys_count');
            $table->dropColumn('views_count');
        });
    }
}
