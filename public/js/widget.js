var retrieveURL = function(filename) {
    var scripts = document.getElementsByTagName('script');
    if (scripts && scripts.length > 0) {
        for (var i in scripts) {
            if (scripts[i].src && scripts[i].src.match(new RegExp(filename+'\\.js$'))) {
                return scripts[i].src.replace(new RegExp('(.*)'+filename+'\\.js$'), '$1');
            }
        }
    }
};

var FXWidget = function() {
    var self = {};

    var re = new RegExp(/^.*\//);
    self.host = re.exec(window.location.href)[0];
    self.host = retrieveURL('js/widget');
    console.log('fx_host', self.host);

    self.init = function(){

        var styleHref = self.host + 'css/widget.css';

        jQuery('head').append('<link rel="stylesheet" href="' + styleHref + '">');
        jQuery('[data-fx-top]:not(.fx_loaded)').each(function(item){
            jQuery.get(self.host + 'api/top', function(html){
                jQuery(this).html(html);
            }.bind(this))
            jQuery(this).addClass('fx_loaded');
            jQuery(this).addClass('alignwide');
        })
        jQuery('body').addClass('fx_loaded');
    }

    // self.init();
    return self;
}

var fxwidget = new FXWidget();

if(typeof jQuery=='undefined') {
    var headTag = document.getElementsByTagName("head")[0];
    var jqTag = document.createElement('script');
    jqTag.type = 'text/javascript';
    jqTag.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js';
    jqTag.onload = fxwidget.init;
    headTag.appendChild(jqTag);
} else {
     fxwidget.init();
}

// jQuery(function(){
//     if(!jQuery('body').hasClass('fx_loaded')){
//
//     }
// })
